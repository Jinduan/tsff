function [  ] = cbFcRs( hf, type )
%CBFCSR Summary of this function goes here
%   Detailed explanation goes here

FcsR = findobj(hf, 'tag', 'FcsR');
FcaR = findobj(hf, 'tag', 'FcaR');

% static model GUI elements
s_group = findobj(hf, 'tag', 'FcsL', '-or', 'tag', 'FcsN');

% adaptive model GUI elements
a_group = findobj(hf, 'tag', 'FcamL', '-or',  'tag', 'FcamN', '-or', ...
     'tag', 'FcamLbL', '-or', ...
     'tag', 'FcahL', '-or',  'tag', 'FcahN', '-or', 'tag', 'FsaC');
 
ud = get(hf, 'userdata');

if type == 1 % adap radio box
    is_a_fc = get(FcaR, 'value');
else
    is_a_fc = ~get(FcsR, 'value');
end

ud{10} = is_a_fc;

if (is_a_fc)  % adaptive model
    set(s_group, 'enable', 'off');
    set(a_group, 'enable', 'on');
    set(FcaR, 'value', 1);
    set(FcsR, 'value', 0);
    cbFsaC(hf);
else
    set(a_group, 'enable', 'off');
    set(s_group, 'enable', 'on');
    set(FcaR, 'value', 0);
    set(FcsR, 'value', 1);
end

% update lookback window size
nLB = lookback(getStrt(hf));
set(findobj(hf, 'tag', 'FcamLbL'), 'string', [' + ' num2str(nLB)]);

set(hf, 'userdata', ud);

end

