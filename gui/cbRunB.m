function [  ] = cbRunB( hf )
%CBRUNB Summary of this function goes here
%   Detailed explanation goes here
tic;

global STOP    % forecasting routine state

ud = get(hf, 'userdata');
RunB = findobj(hf, 'tag', 'RunB');

if strcmp(get(RunB, 'string'), 'Stop') % clicked the ''stop''
    STOP = 1;  % wait for runArima to stop
    return;
end



% assemble template forecaster
training_ratio = getNum(findobj(hf, 'tag', 'FcsN'))/100;
    if training_ratio == 0
        warndlg('Training Ratio must >0');
        return;
    end
mStrt= getStrt(hf);
% lookback_len = lookback(mStrt);

ds = ud{12};
all_data = ds.getIdData();
if size(mStrt, 2) == 1 % non-seasonal
    mu = nanmean(all_data);
    all_data = all_data - mu; % shift average
    disp(['Non-seasonal model - average shifted by: ' num2str(mu)]);
end
training_size = floor(length(all_data)*training_ratio);
fprintf('Training/test set size: %d/%d \n', ...
    training_size, length(all_data)-training_size );

training_data = all_data(1:training_size);
test_data = all_data(training_size + 1:end);

%clc;
% disp(datestr(now));
disp('Model Structure:');
disp(mStrt);
fprintf('Estimating AR parameters...');
[rPara, sigma, ErrorCode] = findSSEmin2(training_data, mStrt);
fprintf('Done.\n');

if ErrorCode
        error('Parameter estimation routine error.');
        %setHFstat(hf, MD); % roll back
end;    

fc_len = getNum(findobj(hf, 'tag', 'FclN'));

fprintf('Building forecaster...');
forecaster = SariForecaster(mStrt, rPara, sigma, fc_len, 0.95);
fprintf('Done.\n');

m = 0; h = 0;  % default glr parameters
if ud{10} == 0 % static
    method = 'static';
else % adaptive
    h = round(getNum(findobj(hf, 'tag', 'FcahN')));
    if get(findobj(hf, 'tag', 'FsaC'), 'value') == 1
        method = 'glr_sigma';
    else
        method = 'glr';
        m = round(str2num(get(findobj(hf, 'tag', 'FcamN'), 'string')));
        if m<=0
            disp('Window size must >0');
            %setHFstat(hf, MD);
            return;
        end
    end  
end

has_viewer = get(findobj(hf, 'tag', 'VrC'), 'value');
SHOW_FIG = get(findobj(hf, 'tag', 'FgC'), 'value');

% run forecaster
STOP = 0;
set(RunB, 'string', 'Stop');

size(all_data)

disp('Forecasting...');
[mErr,mAbsErr,cBounded,nAbn,nSwths,~]=runGLR( ...
    method, ds, forecaster ,m, h, has_viewer, 0, [0.5, 0.95]);


total_res_len = size(mErr, 2);
mErr_r = mErr(:, 1:total_res_len-training_size); % training 
mErr_t = mErr(:, end-training_size+1:end); % test
mAbsErr_r = mAbsErr(:, 1:total_res_len-training_size); % training
mAbsErr_t = mAbsErr(:, end-training_size+1:end); % test
cBounded_1r = cBounded{1}(:, 1:total_res_len-training_size); % training 
cBounded_1t = cBounded{1}(:, end-training_size+1:end);
cBounded_2r = cBounded{2}(:, 1:total_res_len-training_size); % training 
cBounded_2t = cBounded{2}(:, end-training_size+1:end);

%disp('-- Correlation Coefficient ');
horizon = size(mErr, 1);

bi = forecaster.nLB;
% disp(bi)

mPred = repmat(all_data(bi+1+horizon:end), horizon, 1) + mErr;
mPred_r = mPred(:, 1:training_size-bi-horizon);
mPred_t = mPred(:, training_size-bi-horizon+1:end);
rObs_r = training_data(bi+horizon+1:end);
rObs_t = test_data;
np = length(rObs_t);

%save('temp_run');

if SHOW_FIG
    % WRR revision: time series plot
    figure;
    %plot(1:500, rObs_t(1:500), '-', 1:500, mPred_t(1, 1:500), '--'); 
    scatter(rObs_t(1:5000), mPred_t(1, 1:5000));

    % density plot
    [fo, xio] = ksdensity(rObs_t);
    [fp, xip] = ksdensity(mPred_t(1, :));

    figure;
    plot(xio, fo, '-',  xip, fp, '--');

    % ACF plot
    plotAcf(rObs_t, 400, 1, 0);
    plotAcf(mPred_t(1, :)', 400, 1, 0);
    plotAcf(mErr(1, :), 400, 1, 0);
end


% size(rObs_r)
% size(rObs_t)
% size(mPred_r)
% size(mPred_t)

disp('---------------------------------');
disp('Correlation Coefficient (CC, rho)');
disp('---------------------------------');

disp('In Training Data Set:');
mCC_r = zeros(horizon, 1);
for ii = 1:horizon
    cc = corrcoef([rObs_r' mPred_r(ii, :)'], 'rows', 'complete');
    mCC_r(ii) = cc(1,2);
%     disp(cc)
end
disp(mCC_r');

disp('In Test Data Set:');
mCC_t = zeros(horizon, 1);
for ii = 1:horizon
    cc = corrcoef([rObs_t' mPred_t(ii, :)'], 'rows', 'complete');
    mCC_t(ii) = cc(1,2);
end
disp(mCC_t');

disp('Total:');
mCC = zeros(horizon, 1);
for ii = 1:horizon
    cc = corrcoef([[rObs_r rObs_t]', mPred(ii, :)'], 'rows', 'complete');
    mCC(ii) = cc(1,2);
end
disp(mCC');

disp('---------------------------------');
disp('R squared');
disp('---------------------------------');
disp('In Training Data Set:');
%r2 = 1 - nansum(mErr.^2, 2)/nansum((test_data - nanmean(test_data)).^2);
train_r2 = 1 - nansum(mErr_r.^2, 2)/nansum((training_data - nanmean(training_data)).^2);
disp(train_r2');

disp('In Test Data Set:');
test_r2 = 1 - nansum(mErr_t.^2, 2)/nansum((test_data - nanmean(test_data)).^2);
disp(test_r2');

disp('Total:');
r2 = 1 - nansum(mErr.^2, 2)/nansum((all_data - nanmean(all_data)).^2);
disp(r2');


disp('---------------------------------');
disp('Mean absolute relative errors (AARE/MARE/MAPE)');
disp('---------------------------------');

disp('In Training Data Set:');
train_aare = nanmean(mAbsErr_r, 2);
disp(train_aare');
disp('In Test Data Set:');
test_aare = nanmean(mAbsErr_t, 2);
disp(test_aare');
disp('Total:');
disp(nanmean(mAbsErr, 2)');

disp('---------------------------------');
disp('Root Mean Squared errors (RMSE)');
disp('---------------------------------');
disp('In Training Data Set:');
disp(sqrt(nanmean(mErr_r.^2, 2))');
disp('In Test Data Set:');
test_rmse = sqrt(nanmean(mErr_t.^2, 2));
disp(test_rmse');
disp('Total:');
disp(sqrt(nanmean(mErr.^2, 2))');


%disp('Mean absolute errors');
%disp(nanmean(abs(mErr), 2));

disp('---------------------------------');
disp('Observations bounded by predicted probability limits (PICP)');
disp('-- 50% --');

disp('In Training Data Set:');
disp(nanmean(cBounded_1r, 2)');
train_picp50 = nanmean(cBounded_1r, 2);
disp('In Test Data Set:');
picp50 = nanmean(cBounded_1t, 2);
disp(picp50');
disp('Total:');
disp(nanmean(cBounded{1}, 2)');

disp('-- 95% --');

disp('In Training Data Set:');
disp(nanmean(cBounded_2r, 2)');
train_picp95 = nanmean(cBounded_2r, 2);
disp('In Test Data Set:');
picp95 = nanmean(cBounded_2t, 2);
disp(picp95');
disp('Total:');
disp(nanmean(cBounded{2}, 2)');

% Model AIC/BIC 
[ll, n_obs] = logLikelihood(test_data, mStrt, rPara, sigma);
nPara = length(rPara)+1;
aic_score = -2 * ll + 2 * nPara;
bic_score = -2 * ll + log(n_obs) * nPara;

disp('---------------------------------');
disp('Training/test set performance (Lead-1): WRR1-Table S2');
disp('---------------------------------');
disp('             Training          ||          Test             '); 
fprintf('\tR^2\tMAPE\tPICP(50%%)\tPICP(95%%)||\tR^2\tMAPE\tPICP(50%%)\tPICP(95%%)\n');
fprintf('\t %.3f & \t%.2f\\%% & \t%.1f\\%% & \t%.1f\\%% && \t%.3f & \t%.2f\\%% & \t%.1f\\%% & \t%.1f\\%%\n', ...
    train_r2(1), train_aare(1)*100, train_picp50(1)*100, train_picp95(1)*100,...
    test_r2(1), test_aare(1)*100, picp50(1)*100, picp95(1)*100);



disp('---- Test set performance with time horizon ----');
disp(['             Lead-1           ||          Lead-' num2str(fc_len) '             ||      Information Criterion']); 
fprintf('\tAARE\tR^2\tRMSE\tRho\tPICP(95%%)||\tAARE\tR^2\tRMSE\tRho\tPICP(95%%)||\tAIC\tBIC\n');
fprintf(' \t%.2f\\%% & \t%.3f & \t%.3f & \t%.3f & \t%.2f\\%% && \t%.2f \\%%& \t%.3f & \t%.3f & \t%.3f & \t%.2f\\%% &&  \t%.0f & \t %.0f \n', ...
    test_aare(1)*100, test_r2(1), test_rmse(1), mCC_t(1), ...
    picp95(1)*100,...
    test_aare(fc_len)*100, test_r2(fc_len), test_rmse(fc_len), mCC_t(fc_len), ...
    picp95(fc_len)*100, ...
    aic_score, bic_score ...
    );


disp('----- Model Parameters -------');
for ii = 1:length(rPara)
    fprintf('\\phi_{%d}=%.3f, \t', ii, rPara(ii) );
    if mod(ii,8) == 0
        fprintf('$\\\\&$ \n');
    end
end
if size(mStrt, 2) ~= 1
    mu = 0;
end
    
fprintf('\\mu=%0.3f, \t \\sigma=%0.3f\n', mu, sigma);

%disp(['Number of parameter estimation routine abnormalies: ' num2str(nAbn)]);
%disp(['Number of model switches: ' num2str(nSwths)]);



set(RunB, 'string', 'Run');
%setHFstat(hf, MD);

toc

end

