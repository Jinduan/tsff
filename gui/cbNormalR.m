function [ ] = cbNormalR( hf )
%CNORMALR call back function for radio button NormalR
%   Detailed explanation goes here

ud = get(hf, 'userdata');

me = findobj(hf, 'tag', 'NormalR');
TestR = findobj(hf, 'tag', 'TestR');

mode = get(me, 'value');
set(TestR, 'value', ~mode);

ud{5} = mode;

set(hf, 'userdata', ud);
disp(['Set Test(0)/Normal(1) Mode to ' num2str(mode)]); 
%celldisp(get(hf, 'userdata'));



end

