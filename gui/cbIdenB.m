function [ ] = cbIdenB( hf )
%CBIDENB call back function of Identfy button
%   
ud = get(hf, 'userdata');
ds = ud{12};

if isempty(ds), return, end; % no datasource

nTp = getNum(findobj(hf, 'tag', 'TPN')); % Total No. of AR parameters
if nTp==0
    disp('Must have at least one AR parameter');
    setHFstat(hf, MD);
    return; % cann't optimize
end

% find optimal model structure given the preriods entered in the GUI
[mStrt0] = getStrt(hf);
[rTs_f, ~] = diffTs(ds.getIdData(), mStrt0);
rP = identifySarima(rTs_f, mStrt0(1,:), nTp, 'bic');

% fill back controls

for iC = 1:4
    if iC == 1 || get(findobj(hf, 'tag', ['S' num2str(iC) 'C']), 'value') == 1

        set(findobj(hf, 'tag', ['P' num2str(iC) 'N']), ...
            'string', num2str(rP(iC)));
    end
end

% set display of lookback length
nLB = lookback(mStrt0);
set(findobj(hf, 'tag', 'FcamLbL'), 'string', [' + ' num2str(nLB)]);


end

