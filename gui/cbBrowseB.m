function [ ] = cbBrowseB( hf )
%CBBROWSEB Summary of this function goes here
%   Detailed explanation goes here

ud = get(hf, 'userdata');


[fileName, pathName, filterIndex] = uigetfile( ...
            {'*.mat', 'MATLAB saved matrixes'; ...
            '*.xlsx;*.xls', 'Excel spreadsheet'; ...
            '*.dsn', 'Database connection string'}, ...
          'Select a data source file');

DsT = findobj(hf, 'tag', 'DsT');
DsL = findobj(hf, 'tag', 'DsL');
DsB = findobj(hf, 'tag', 'DsB');

ud{6} = filterIndex + 1;

if fileName~=0
    set(DsT, 'string', [pathName filesep fileName]);
    set(DsL, 'string', [get(DsL, 'userdata'), ud{7}{ud{6}}]);
    set(DsB, 'enable', 'on');
else
    set(DsT, 'string', '');
    set(DsL, 'string', [get(DsL, 'userdata'), ud{7}{1}]);
    set(DsB, 'enable', 'off');
end
    
set(hf, 'userdata', ud);


end

