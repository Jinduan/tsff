function [ mStrt] = getStrt( hf )
%GETSTRT get the model structure and look-back length from the gui
%   

d1n = findobj(hf, 'tag', 'D1N');
p1n = findobj(hf, 'tag', 'P1N');
mStrt = [1; getNum(p1n); getNum(d1n); 0];

for iC = 2:4
    if get(findobj(hf, 'tag', ['S' num2str(iC) 'C']), 'value') == 1
        s = getNum(findobj(hf, 'tag', ['S' num2str(iC) 'N']));
        d = getNum(findobj(hf, 'tag', ['D' num2str(iC) 'N']));
        p = getNum(findobj(hf, 'tag', ['P' num2str(iC) 'N']));

        %if s*d == 0
        %    continue; % no seasonallity in this level
        %end
        mStrt = [mStrt [s;p;d;0]];
    end
end
      

end

