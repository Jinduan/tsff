function [  ] = cbDsB( hf )
%CBDSB Summary of this function goes here
%   Detailed explanation goes here


me = findobj(hf, 'tag', 'DsB');
   
% 'Set'    

ud = get(hf, 'userdata');
file = get(findobj(hf, 'tag', 'DsT'), 'string');
if ~exist(file, 'file')
    warndlg('Data source file not exist.')
    return
end

% calculate sum ratio -> account for source/system time interval difference
insecs = [1 60 3600 3600*24];
SrcdtN = findobj(hf, 'tag', 'SrcdtN');
SysdtN = findobj(hf, 'tag', 'SysdtN');
SrcdtU = findobj(hf, 'tag', 'SrcdtU');
SysdtU = findobj(hf, 'tag', 'SysdtU');

r = getNum(SysdtN) * insecs(get(SysdtU, 'value')) / ...
    (getNum(SrcdtN) * insecs(get(SrcdtU, 'value')) );
if r<= 0  % wrong inputs
    warndlg('both source and system time intervals should be positive');
    return;
end

ds = {};
data_series = [];  % Raw data series

switch ud{6}
    case 2  % mat
        ds = EmuDataSource(file, 0, r);
        
        in = load(file);
        fn = fieldnames(in);
        data_series = in.(fn{1});
        data_series = reshape(data_series, [], 1);
        
        [data_count, missing_data_count, data_mean,  ...
                data_variance, data_max, data_min] = ds.getDataInfo();
        GiL = findobj(hf, 'tag', 'GiL');
        set(GiL, 'string', ...
            ['Data Source: ' num2str(data_count) ' points, ' ...
            num2str(missing_data_count) ' missing (' ...
            num2str(missing_data_count/data_count*100) ...
            '%). mean/std/min/max = ' num2str(data_mean) ...
            ' / ' num2str(sqrt(data_variance)) ' / ' num2str(data_min) ' / ' ...
            num2str(data_max)]);
%             
    case 3  % xls
        ds = readExl(file, r);
        warndlg('Excel file support is not available in this version.');
        return;
    case 4  % db
        warndlg('Database support is not available in this version.');
        return;

    otherwise  % unknown
        warndlg('Unknown data source');
        return;
end

if strcmp(ds.getStatus, 'error')
    warndlg('Error loading file or setting source');
    return;
end


% normal
disp(['Data source OK, Length:' num2str(ds.getLen())]);
global STOP
STOP = 0;

RunB = findobj(hf, 'tag', 'RunB');
set(RunB, 'string', 'Run');


ud{12} = ds;
ud{13} = data_series;

set(hf, 'userdata', ud);




end

