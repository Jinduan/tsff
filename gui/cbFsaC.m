function [ ] = cbFsaC( hf )
%CBFSAC Summary of this function goes here
%   Detailed explanation goes here

only_adj_var = get(findobj(hf, 'tag', 'FsaC'), 'value');
m_group = findobj(hf, 'tag', 'FcamL', '-or', 'tag', 'FcamN', ...
    '-or', 'tag', 'FcamLbL');

if only_adj_var
    set(m_group, 'enable', 'off');
else
    set(m_group, 'enable', 'on');
end
    


end

