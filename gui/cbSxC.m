function [  ] = cbSxC( hf, x )
%CBSXC Summary of this function goes here
%   Detailed explanation goes here

xstr = num2str(x);
me = findobj(hf, 'tag', ['S' xstr 'C']);

sl = findobj(hf, 'tag', ['S' xstr 'L']);
sn = findobj(hf, 'tag', ['S' xstr 'N']);
dl = findobj(hf, 'tag', ['D' xstr 'L']);
dn = findobj(hf, 'tag', ['D' xstr 'N']);
pl = findobj(hf, 'tag', ['P' xstr 'L']);
pn = findobj(hf, 'tag', ['P' xstr 'N']);

if get(me, 'value')==0  
    set([sl, sn, dl, dn, pl, pn], 'enable', 'off'); %disable this seasonality
else
    set([sl, sn, dl, dn, pl, pn], 'enable', 'on');
end
    
    


end

