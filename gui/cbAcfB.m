function [  ] = cbAcfB( hf )
%CBACFB call back func of the "plot acf_pacf" button  

ud = get(hf, 'userdata');

mStrt= getStrt(hf);

% [file_name, path_name,~] = uigetfile('*.mat', ...
%     'Data for plotting ACF/PACF charts');
% full_file_name = [path_name filesep file_name];
% if ~exist(full_file_name, 'file')
%     return; 
% end

%vars = load(full_file_name);
ts = ud{13};

% Maximum acf/pacf computed
nLB = lookback(mStrt);
max_acf_lag = min(round((length(ts) - nLB)/8), 2000);
%max_acf_lag = round((length(ts) - nLB)/8);

disp('Calculate ACF/PACF, this may take a few minutes...');
[data_filtered, ~] = diffTs(ts, mStrt);
plotAcf(data_filtered, max_acf_lag, 1, 1);


end

