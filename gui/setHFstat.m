function [  ] = setHFstat( hf, stat )
%SETHFSTAT Setup handle figure's status
%   Detailed explanation goes here

ud = get(hf, 'userdata');
set(findobj(hf, 'type', 'uicontrol'), 'enable', 'off');


global WAIT DS MD RUN

if stat==WAIT || stat==DS || stat==MD || stat==RUN

        set(hf, 'name', [ud{1} ud{3}{stat}]);
        for iCtrl = 1:length(ud{11}{stat})
            set(findobj(hf, 'tag', ud{11}{stat}{iCtrl}), 'enable', 'on');
        end
        
        ud{2} = stat;
   
end

if stat==MD
    if ud{10} == 0 % static fc
        set(findobj(hf, 'tag', 'FcsL', '-or', 'tag', 'FcsN'), 'enable', 'on');
    else % adaptive fc
        set(findobj(hf, 'tag', 'FcamL', '-or', 'tag', 'FcamN', '-or', ...
            'tag', 'FcahL', '-or', 'tag', 'FcahN', '-or', 'tag', 'FcamLbL'), 'enable', 'on');
    end
end
        
    

set(hf, 'userdata', ud);
drawnow;        
        
        

end

