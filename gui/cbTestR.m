function [ ] = cbTestR( hf )
%CBTESTR call back function for radio button testR
%   Detailed explanation goes here

ud = get(hf, 'userdata');

me = findobj(hf, 'tag', 'TestR');
NormalR = findobj(hf, 'tag', 'NormalR');

mode = ~get(me, 'value');
set(NormalR, 'value', mode);

ud{5} = mode;

set(hf, 'userdata', ud);
disp(['Set Test(0)/Normal(1) Mode to ' num2str(mode)]); 
%celldisp(get(hf, 'userdata'));


end

