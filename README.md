Time Serires Forecasting Framework (TSFF) for multi-seasonal autogressive integrated modeling (msARI)

Reference: Real-time forecasting and visualization toolkit for multi-seasonal time series. J Chen and DL Boccelli (2018). Environmental Modelling and Software. 105(2018), 244-256.
https://doi.org/10.1016/j.envsoft.2018.03.034

Developers: Jinduan Chen and Dominic L. Boccelli

Contact address: 737 Engineering Research Center, University of Cincinnati, Cincinnati, OH 45221-0012, USA

Year first available: 2014

Hardware required: PC

Software required: Matlab 7.12.0 (R2011a)

License: GNU General Public License, Version 2. 
https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
