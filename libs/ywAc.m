function [ rAc ] = ywAc( mStrt, rPara, nac )
%YWAC Compute theoretical autocorrelations with Yule-Walker Equation
%  Ignore non-stationarity (d-row of mstrt)
%   Inputs:
%       mStrt - matrix of msARI model structure
%       rPara - row vector of autoregressive parameters
%       nac   - number of autocorrelation coefficients desired
%   Returns:
%       rAc - row vector of autocorrelation coefficients

if nargin < 3
    error('Not enough arguments: mStrt, rPara, nac');
end

mStrtS = mStrt;
mStrtS(3, :) = 0;
[rPhi, ~] = strt2poly(mStrtS, rPara);
n = length(rPhi);

rAc = zeros(1, max(nac, n-1));

% first p parameters
cRho = (toeplitz(-rPhi(1:n-1), [-1 zeros(1,n-2)]) + ...
        hankel([-rPhi(3:end) 0], zeros(1,n-1)))\rPhi(2:end)';

rAc(1:n-1) = cRho';
rPhiR = fliplr(-rPhi(2:end)); % reverse direction

for iAc = n:nac
    rAc(iAc) = dot(rPhiR, rAc(iAc-(n-1):iAc-1) );
end


end

