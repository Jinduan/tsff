classdef RTviewer < handle
    %RTVIEWER real-time viewer for water demand forecasting
    %   The real-time viewer provides 2 types of perspectives:
    %   "modeler" -- has views of raw data stream, forecast diagram, and hindcast
    %               diagram on the 1st panel (figure); error matrix and error
    %               histogram on the 2nd panel
    %   "operator" -- has views of raw data stream, forecast/hindcast diagram,
    %               and error matrix on a single panel
    
    properties
        %%
        % file(module) name
        MN = 'TRVIEWER:';  % Module name
        
        % mode
        mode = 'modeler';
        
        % display switch for RDV, RDB, FCV, HCV, EMV
        showable = 0;
        
        % initial time, current time
        init_time = 0
        cur_time = 0
        
        % buffer for the raw data stream
        dat_buf
        
        % # of observations displayed in the raw data view, = length(dat_buf)
        top_win_width = 200;
        
        % # of observations displayed in the raw data bar chart, should < top_win_width
        rdb_width = 24 * 3;  % 7 days of hourly demands
        
        % # of observations displayed in the forecast and hindcast views
        bot_win_width = 40;
        
        % forecasting length. This value is also the size of error matrix view
        % forecast view's width = bot_win_width + fc_win_width
        fc_win_width = 10;
        
        % y-axis limits for the views, default value
        win_ylims = [0 2];
        
        % figures
        hf0
        hf1
        hf2
        hf3
        hf4 = 0;% for GLRAV
        hf5 = 0;% for GLR parameter view
        
        % raw data bar view (RDB)
        h0          % axis
        hdbs        % raw data bar series
        
        % raw data view (RDV)
        h1          % axis
        hdln1       % raw data line
        hcur_mk1    % current observation
        ht1         % current observation - text
        hr          % red box (zoom-in view)
        
        % forecast and hindcast view (FCHCV)
        h2          % axis
        hdln2       % observations
        hfln2       % forecasts
        hcln2_1U    % upper confidence interval - 1
        hcln2_1L    % lower c.i. -1
        hcln2_2U
        hcln2_2L
        hvl2        % current time marker
        
        h3
        hdln3
        hfln3
        hcln3_1U
        hcln3_1L
        hcln3_2U
        hcln3_2L
        hvl3
        
        % error matrix view (EMV)
        
        % buffer for the abs error calculation
        % three-day errors are stored in the buffer, should < top_win_width
        ABS_ERR_BUFFER_LENGTH = 24 * 3;
        abs_err_buf
        ha2         % axis
        hi          % matrix
        texts       % text object matrix
        
        % error histogram view (EHV)
        max_lead_time = 1;      % lead-1 ,lead-2, and lead-3 errors are considered.
        
        SQR_ERR_BUFFER_LENGTH = 1000;
        sqr_err_buf             % buffer for squared relative error histogram display
        pseb = 0;               % square error buffer pointer
        he1                     % axis
        edges = [0:0.05:1];     % edges for the histogram
        
        % GLR alternative model view (GLRAV)
        hamx;              % axis
        hl_orig;            % line for the orginal model
        hl_alt;             % line for the alternative model
        GLRAV_BUFFER_LENGTH = 720*10;  % ten months
        orig_score_buf;        % buffer for the original model's score
        alt_score_buf;      % buffer for the alternative model's score
        
        % GLR parameter view (GLRP)
        hmp;                % axis
        
    end
    
    
    methods
        %% constructor: set up the viewer
        function [this] = RTviewer(mode, init_time, tww, bww, fww, ylims)
            if nargin > 5
                this.win_ylims = ylims;
            elseif nargin > 4
                this.fc_win_width = fww;
                if fww < 3
                    this.max_lead_time = fww;
                end
            elseif nargin > 3
                this.bot_win_width = bww;
            elseif nargin > 2
                this.top_win_width = tww;
            elseif nargin < 2
                throw(MException([this.MN 'Arg2few'], ...
                    'Arguments "mode" and "init_time" are mandatory.'));
            end
            
            disp('Constructing a real-time viewer...');
            this.init_time = init_time;
            this.mode = mode;
            if strcmp(mode, 'operator')
                
                this.hf1 = figure('Name', 'Time series data and forecasts');
                this.h1 = subplot(311);
                
                xlabel('Elapsed time (hours)');
                ylabel({'Demand multiplier' '(ratio of real demand' 'to avg. demand)'});
                this.h2 = subplot(3, 4, [5 6]);
                xlabel('Elapsed time (hours)'); ylabel('Demand multiplier');
                this.h3 = subplot(3, 4, [9 10]);
                xlabel('Elapsed time (hours)'); ylabel('Demand multiplier');
                this.ha2 = subplot(3, 4, [7 8 11 12]);
                xlabel('Error matrix');
                
                
            else  % modeler's perspective
                this.hf0 = figure('Name', 'Time series data: bar chart');
                
                % set up raw data bar charts
                
                this.h0 = axes;
                
                tp_mat = NaN * zeros(24, 3); % temporary matrix
                this.hdbs = bar3(this.h0, tp_mat, 1);
               
                colormap(this.h0, winter);
                
                
                this.hf1 = figure('Name', 'Time series data and forecasts');
                
                % set up raw data view
                this.h1 = subplot(311);
                xlabel('Elapsed time (hours)');
                %ylabel({'Demand multiplier' '(ratio of real demand' 'to avg. demand)'});
                ylabel('Demand (MGD)');
                
                % set up forecast/hindcast view
                this.h2 = subplot(313);
                xlabel('Elapsed time (hours)');
                ylabel('Demand (MGD)');
                this.h3 = subplot(312);
                xlabel('Elapsed time (hours)');
                ylabel('Demand (MGD)');
                
                this.hf2 = figure('Name', 'Error matrix');
                
                % set up error matrix view
                this.ha2 = axes('parent', this.hf2);
                xlabel('Error matrix');
                
                % set up error histogram
                this.hf3 = figure('Name', 'Error histogram');
                this.he1 = axes('parent', this.hf3);
                xlabel('Absolute Relative Error');
                ylabel('Percentage');
                
            end
            
            set([this.h1 this.h2 this.h3], 'ylim', this.win_ylims);
            set([this.h1 this.h2 this.h3], 'FontSize', 8, 'xgrid', 'off', 'ygrid', 'on');
            
            % create graphic objects
            % raw data stream line
            this.hdln1 = line;
            set(this.hdln1, 'parent', this.h1, 'linestyle', '-', 'color', 'k');
            this.ht1 = text(0, 0, '', 'parent', this.h1, 'fontsize', 7, ...
                'verticalalignment', 'top', 'horizontalalignment', 'center');
            this.hcur_mk1 = line('parent', this.h1, ...
                'linestyle', 'none', 'marker', 'o', 'color', 'r');
            
            % data lines in forecast/hindcast views
            this.hdln2 = line;
            set(this.hdln2, 'parent', this.h2, 'linestyle', 'none', 'marker', 'x', 'color', 'k');
            this.hdln3 = copyobj(this.hdln2, this.h3);
            
            % create line objects for forecasts and confidence limits
            this.hfln2 = line;  % forecast line for the middle
            set(this.hfln2, 'parent', this.h2, 'linestyle', '-', 'marker', 'o', 'color', 'b');
            this.hfln3 = copyobj(this.hfln2, this.h3);
            set([this.hdln2, this.hdln3, this.hfln2, this.hfln3], 'markersize', 5);
            this.hcln2_1U = line(1:this.fc_win_width, 1:this.fc_win_width);
            set(this.hcln2_1U, 'parent', this.h2, 'linestyle', '--', 'color', 'm');
            this.hcln2_1L = copyobj(this.hcln2_1U, this.h2);
            this.hcln2_2U = copyobj(this.hcln2_1U, this.h2);
            set(this.hcln2_2U, 'color', [0 .5 0]);
            this.hcln2_2L = copyobj(this.hcln2_2U, this.h2);
            this.hcln3_1U = copyobj(this.hcln2_1U, this.h3);
            this.hcln3_1L = copyobj(this.hcln2_1U, this.h3);
            this.hcln3_2L = copyobj(this.hcln2_2U, this.h3);
            this.hcln3_2U = copyobj(this.hcln2_2U, this.h3);
            
            % create zoom-in box and two vertical lines
            this.hr = rectangle;
            set(this.hr, 'parent', this.h1, 'edgecolor', 'r', 'linewidth', 2);
            this.hvl2 = line; set(this.hvl2, 'parent', this.h2, 'linestyle', '-', 'color', 'r');
            this.hvl3 = copyobj(this.hvl2, this.h3);
            
            % Create an error diagram
            
            fcw = this.fc_win_width;
            empty_img = zeros(fcw, fcw+1);
            this.hi = imagesc(empty_img, 'parent', this.ha2, this.win_ylims);
            
            %colorbar('peer', this.ha2, 'horizontal');
            set(this.ha2, 'xaxislocation', 'top', 'fontsize', 8);
            set(this.ha2, 'yaxislocation', 'right');
            set(this.ha2, 'ytick', 1:this.fc_win_width);
            set(this.ha2, 'yticklabel', {});
            set(this.ha2, 'ticklength', [0 0]);
            %xlabel(this.ha2, 'Time axis ->');
            %ylabel(this.ha2, 'Forecasting lead time');
            
            rectangle('parent', this.ha2, 'edgecolor', 'r', 'linewidth', 2, ...
                'position', [this.fc_win_width+0.5 0.55 0.95 this.fc_win_width-.1]);
            % create text objects
            this.texts = cell(size(empty_img));
            for ii = 1:size(this.texts, 2)
                for jj = 1:size(this.texts, 1)
                    this.texts{ii, jj} = text(ii, jj, '0.00', 'parent', this.ha2, ...
                        'fontsize', 7, 'horizontalalignment', 'center');
                    if ii+jj < 12
                        set(this.texts{ii, jj}, 'color', 'k');
                    end
                end
            end
            
            ylims = [0 1];
            
            set(this.he1, 'nextplot', 'add', 'xlim', [0 1], 'ylim', ylims);
            
            % initialize data buffers
            this.dat_buf = zeros(this.top_win_width, 1);
            this.abs_err_buf = zeros(this.fc_win_width, this.ABS_ERR_BUFFER_LENGTH);
            this.sqr_err_buf = zeros(this.SQR_ERR_BUFFER_LENGTH, this.max_lead_time);
            
            this.cur_time = this.init_time;
            
            disp(['A real-time viewer has been built, initial time: ' ...
                num2str(this.init_time) 'H, displayable time: ' ...
                num2str(this.init_time + this.top_win_width)]);
            
            % don't show anything at this moment (except EDV), 
            % wait until dat_buf is full
            set(this.hf0, 'visible', 'off');
            set(this.hf1, 'visible', 'off');
            if strcmp(this.mode, 'modeler')
                set(this.hf2, 'visible', 'off');
            end
            
            set([this.hf0; this.hf1; this.hf2; this.hf3], 'color', 'w');
        end
        
        function [] = destroy(this)
            hfs = [this.hf0, this.hf1, this.hf2, this.hf3, this.hf4, this.hf5];
            for i_hf = 1:length(hfs)
                if hfs(i_hf)>0 && ishandle(hfs(i_hf))
                    close(hfs(i_hf));
                end
            end
        end
        
        %% update everything, this func is a public interface
        function [] = update(this, cd, fc1, fc2, hc1, hc2, fclist)
            % RT viewer update routine
            % INPUTS:   cd - current observation
            %           fc1, fc2 - forecasters with two different CI settings
            %           hc1, hc2 - hindcasters
            %           fclist - list of forecasters (for error matrix view)
            if ~isscalar(cd)
                throw(MException([this.MN 'InputErr'], ...
                    'Arguments "cd" should be a scalar'));
            end
%             if ~all([isa(fc1, 'ArimaLSForecaster'), isa(fc2, 'ArimaLSForecaster'), ...
%                     isa(hc1, 'ArimaLSForecaster'), isa(hc2, 'ArimaLSForecaster')])
%                 throw(MException([this.MN 'InputErr'], ...
%                     'Arguments fc1, fc2, hc1, hc2 must be Forecasters.'));
%             end
            if ~isa(fclist, 'cell')
                throw(MException([this.MN 'InputErr'], ...
                    'Arguments fclist must be a cell array.'));
            end
            if length(fclist) < 2
                throw(MException([this.MN 'InputErr'], ...
                    'Arguments fclist must have at least 2 members.'));
            end
%             for ii = 1:length(fclist)
%                 if ~isa(fclist{ii}, 'ArimaLSForecaster')
%                     throw(MException([this.MN 'Arg2few'], ...
%                         'Each member of fclist must be a Forecaster.'));
%                 end
%             end
            
            
            this.dat_buf = [this.dat_buf(2:end); cd];
            this.cur_time = this.cur_time + 1;
            this.win_ylims = [min(this.dat_buf(:)), max(this.dat_buf(:))];
            
            flag_dis = updateRDV(this, cd);
            if (flag_dis == 1 && this.showable == 0)
                this.showable =1;
                set(this.hf0, 'visible', 'on');
                set(this.hf1, 'visible', 'on');
                if strcmp(this.mode, 'modeler')
                    set(this.hf2, 'visible', 'on');
                end
                %tileFigs();
            end
           
            if strcmp(this.mode, 'modeler')
                updateEHV(this, cd, fclist);
            end
            
            if this.showable
                %                fprintf('x');
                updateRDB(this);
                updateRCHCV(this, fc1.getFCs(), fc1.getLlims(), fc1.getUlims(), ...
                    fc2.getLlims(), fc2.getUlims(), hc1.getFCs(), hc1.getLlims(), ...
                    hc1.getUlims(), hc2.getLlims(), hc2.getUlims());
                updateEMV(this, fclist);
                
            end
            
            drawnow();
            
        end
        
        %% update raw data view
        function [displayable] = updateRDV(this, raw_data)
            % INPUT: raw_data -- a scalar representing the most current observation
            
            if this.cur_time - this.init_time < this.top_win_width
                % not enough data to display
                displayable = 0;
                return;
            end
            
            ct = this.cur_time; cd = raw_data;
            
            xlim = [ct - this.top_win_width + 1   ct + this.fc_win_width];
            set(this.h1, 'xlim', xlim);
            set(this.h1, 'ylim', this.win_ylims);
            set(this.hdln1, 'xdata', ct - this.top_win_width + 1 : ct, ...
                'ydata', this.dat_buf);
            set(this.ht1, 'position', [ct this.win_ylims(2)], 'string', ...
                {['Current Time : ' num2str(floor(ct/24), '%3.0f') 'D ' num2str(mod(ct, 24)) 'H'], ...
                ['Data: ' num2str(cd, '%8.4f')]});
            set(this.hcur_mk1, 'xdata', ct, 'ydata', cd);
            
            set(this.hr, 'Position', ...
                [xlim(2)-this.bot_win_width, this.win_ylims(1), ...
                this.bot_win_width, range(this.win_ylims)]);
            
            displayable = 1;
            return
            
        end
        
        %% update raw data bar chart
        function [] = updateRDB(this)
            s1 = 24;
            s2 = 168/24;
            
            cur_day = floor((this.cur_time-1)/s1) + 1 ;
            cur_hour = mod(this.cur_time - 1, s1);
            
            tp_mat = [this.dat_buf(end + 1 - (s2)*s1 + (s1 - 1 - cur_hour):end); ...
                NaN * zeros( s1 - 1 -cur_hour, 1)];
            tp_mat = reshape(tp_mat, s1, s2);
            tp_mat = tp_mat(:, end:-1:1);
            
            this.hdbs = bar3(this.h0, tp_mat, 1);
%             
%             for i = 1:length(this.hdbs)
%                 zdata = get(this.hdbs(i),'ZData');
%                 set(this.hdbs(i),'CData',zdata )
%                 % Add back edge color removed by interpolating shading
%                 set(this.hdbs,'EdgeColor','k')
%             end
            colormap(this.hf0, 'jet');
            
            for k = 1:length(this.hdbs)
                zdata = max(get(this.hdbs(k), 'ZData'),[], 2);
                set(this.hdbs(k),'CData', repmat(zdata, 1, 4));
                %set(this.hdbs(k),'FaceColor','interp');
            end
%             
            set(this.h0, ...
                'xlim', [0.5 s2+0.5], 'ylim', [0.5 s1 + 0.5], 'zlim', [0 this.win_ylims(2)], ...
                'xtick', 1:s2, 'ytick', 1:4:s1, 'xticklabelmode', 'manual', ...
                'yticklabelmode', 'manual', ...
                'yticklabel', cellstr(num2str([0:4:s1]')));
            set(this.h0, 'xticklabel', cellstr(num2str([cur_day:-1:cur_day-s2]')));
            
            xlabel(this.h0, 'Day');
            ylabel(this.h0, 'Hour in a day');
            zlabel(this.h0, 'Relative water demand');
            set(this.h0, 'view', [-55 30]);
        end
            
        
        %% update forecast and hindcast view
        function [] = updateRCHCV(this, fcs, fc_llims1, fc_hlims1, fc_llims2, fc_hlims2, ...
                hcs, hc_llims1, hc_hlims1, hc_llims2, hc_hlims2)
            
            ct = this.cur_time;
            fww = this.fc_win_width; %displayed win width
            xlim = [ct-this.bot_win_width+1 ct+this.fc_win_width];
            set([this.h2 this.h3], 'xlim', xlim);
            set([this.h2 this.h3], 'ylim', this.win_ylims);
            set([this.hdln2 this.hdln3], 'xdata', xlim(1):ct, ...
                'ydata', this.dat_buf(end-this.bot_win_width+1:end));
            
            % current-time marker line
            set([this.hvl2 this.hvl3], 'xdata', [ct; ct], 'ydata', this.win_ylims');
            
            % plot forecasts and CIs
            set([this.hfln2, this.hcln2_1U, this.hcln2_1L, this.hcln2_2U, this.hcln2_2L], ...
                'xdata', ct-this.fc_win_width+1:ct);
            set([this.hfln3, this.hcln3_1U, this.hcln3_1L, this.hcln3_2U, this.hcln3_2L], ...
                'xdata', ct+1:ct+this.fc_win_width);
            set(this.hfln2, 'ydata', hcs(1:fww));
            set(this.hfln3, 'ydata', fcs(1:fww));
            set(this.hcln2_1U, 'ydata', hc_hlims1(1:fww));
            set(this.hcln2_1L, 'ydata', hc_llims1(1:fww));
            set(this.hcln2_2U, 'ydata', hc_hlims2(1:fww));
            set(this.hcln2_2L, 'ydata', hc_llims2(1:fww));
            set(this.hcln3_1U, 'ydata', fc_hlims1(1:fww));
            set(this.hcln3_1L, 'ydata', fc_llims1(1:fww));
            set(this.hcln3_2U, 'ydata', fc_hlims2(1:fww));
            set(this.hcln3_2L, 'ydata', fc_llims2(1:fww));
            
        end
        
        %% update error matrix
        function [] = updateEMV(this, fcts)
            % Update forecasting map / err diagram
            ct = this.cur_time;
            fcw = this.fc_win_width;
            empty_img = zeros(fcw, fcw+1);
            fcs_img = empty_img;
            err_img = empty_img;
            
            set(this.ha2, 'xtick', 1:2:fcw+1);
            set(this.ha2, 'xticklabel', ...
                num2str(mod([ct-fcw:2:ct]', 24), '%2.0fH'));
            
            for kk = 1:length(fcts)
                
                fcs = fcts{kk}.getFCs();
                
                if kk>1
                    fcs_img(fcw+2-kk:end, kk) = ...
                        fcs(fcw+2-kk:fcw);
                    for hh = fcw+2-kk:fcw
                        set(this.texts{kk, hh}, 'string', num2str(fcs(hh), '%4.2f'));
                    end
                end
                if kk < length(fcts)
                    % calculate relative squared error
                    for hh = 1:fcw-kk+1
                        err_img(hh, kk) = (fcs(hh)-this.dat_buf(end-fcw+hh+kk-1)) ...
                            /this.dat_buf(end-fcw+hh+kk-1);
                        set(this.texts{kk, hh}, 'string', num2str(err_img(hh, kk)*100, '%4.1f%%'));
                        if abs(err_img(hh, kk)) > 0.1
                            err_img(hh, kk) = prctile(this.dat_buf, 95);
                        elseif abs(err_img(hh, kk)) > 0.05
                            err_img(hh, kk) = prctile(this.dat_buf, 80);
                        else
                            err_img(hh, kk) = prctile(this.dat_buf, 10);
                        end
                    end
                    % update moving-average relative abs error buffer
                    this.abs_err_buf(fcw+1-kk, mod(ct-fcw+kk, this.ABS_ERR_BUFFER_LENGTH)+1) = ...
                        abs(fcs(fcw+1-kk)-this.dat_buf(end))/this.dat_buf(end);
                end
                
            end
            set(this.hi, 'cdata', fcs_img+err_img);
            set(this.ha2, 'clim', this.win_ylims);
            if (ct-this.top_win_width > this.ABS_ERR_BUFFER_LENGTH)
                set(this.ha2, 'yticklabel', ...
                    num2str(nanmean(this.abs_err_buf, 2).*100, '%4.1f%%'));
            end
            
        end
        
        %% update error histogram
        function [] = updateEHV(this, cd, fclist)
            
            
            ct = this.cur_time;
            ci = this.init_time;
            
            % update square root error buffer
            for ii = 1:this.max_lead_time
                fcs = fclist{end-ii}.getFCs();
                this.sqr_err_buf(mod(ct-ci, this.SQR_ERR_BUFFER_LENGTH)+1,ii)...
                    = sqrt((1-fcs(ii)/cd)^2);
            end
            if this.pseb < this.SQR_ERR_BUFFER_LENGTH
                this.pseb = this.pseb + 1;
            end
            
            % draw the error histogram
            cla(this.he1);
            legendstr = cell(this.max_lead_time, 1);
            for ii=1:this.max_lead_time
                proportions = histc(this.sqr_err_buf(1:this.pseb, ii), ...
                    this.edges)/this.pseb;
                htp = bar(this.he1, this.edges, proportions, 'histc');
                cm = jet(this.max_lead_time);
                set(htp, 'facecolor', cm(ii, :));
                legendstr{ii} = ['Lead-' num2str(ii) ' sqrt. forecasting error'];
            end
            legend(this.he1, legendstr);
            %            fprintf('o');
            
            
        end
        
        %% update GLRAV
        function [] = updateGLR(this, score_orig, score_alt, switch_flag)
            if (this.hf4 == 0) 
                % initialize GLR view
                this.hf4 = figure('Name', 'GLR model switching view');
                this.hamx = axes('Parent', this.hf4);
                this.hl_orig = line('Parent', this.hamx, ...
                    'color', 'red', 'Marker', 'none', 'linestyle', '-');
                this.hl_alt = line('Parent', this.hamx, ...
                    'color', 'blue', 'Marker', 'none', 'linestyle', '-');
                xlabel(this.hamx, 'Time step');
                ylabel(this.hamx, 'Negative log-likelihood');
                
            end
            
            % update buffer
            this.orig_score_buf = [this.orig_score_buf, score_orig];
            this.alt_score_buf = [this.alt_score_buf, score_alt];
            if length(this.orig_score_buf)>this.GLRAV_BUFFER_LENGTH
                this.orig_score_buf(1) = [];
                this.alt_score_buf(1) = [];
            end
            
            % update GLs
            set(this.hl_orig, 'XData', this.cur_time-length(this.orig_score_buf)+1:this.cur_time,...
                 'YData', this.orig_score_buf);
            set(this.hl_alt, 'XData', this.cur_time-length(this.alt_score_buf)+1:this.cur_time, ...
                 'YData', this.alt_score_buf);
             
            % mark switch point
            
            if switch_flag
                line('Parent', this.hamx, ...
                    'xdata', this.cur_time, 'ydata', score_alt, ...
                    'marker', 'o', 'color', [0 1 0.5]);
            end
            drawnow();
            
        end
        
        %% dryrun - don't update any graphics
        function [] = dryrun(this)
            this.cur_time = this.cur_time + 1;
        end
        
        %% update GLR parameter view
        function [] = updateGLRP(this, phi, theta, sigma, switch_flag)
            if length(phi) == 2 && length(theta) == 0 % only AR(2) para view is implemented
                if (this.hf5 == 0)
                    % initialize GLRP view
                    this.hf5 = figure('Name', 'GLR model parameter view');
                    this.hmp = axes('Parent', this.hf5);
                    set(this.hmp, 'xlim', [-2 2], 'ylim', [-2 2]);

                end
                
                if switch_flag
                    line('parent', this.hmp, 'xdata', phi(1), 'ydata', phi(2), ...
                        'Marker', 'x', 'color', 'black', 'markersize', 8);
                end
                
                
            end
            
        end
        
    end
    
end


