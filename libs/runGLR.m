function [mErr,mAbsErr,cBounded,nAbn,nSwths,rLikelihood] = runGLR(mode,datasrc,forecaster,m,h,bViewer,bLogData,PL)
%RUNGLR forecast a time series using SariForecaster: GLR algorithm
%   
% Inputs:
%   mode - 
%       'glr' mode: using sliding-window GLR.
%       'glr_sigma' mode: using general GLR scheme, but only on parameter sigma.
%       'static' mode: using pre-determined fixed dsARI model. no GLR
%           switching.
%   datasrc - Data source. must have getStatus() and getAnObs() methods
%       if in test mode, must have getLen() method
%   forecaster - forecaster instance used
%       contains the model structure, max forecasting horizon,
%       and initial parameter selection
%   m - GLR algorithmic parameter. size of the estimation moving window.
%   this parameter is only applicable for 'test' mode (fixed-window GLR).
%   h - GLR algorithmic parameter. threshold of model switching
%   bViewer - if there is a online viewer of the algorithm
%           bViewer == 0: no viewer
%           bViewer == 1: normal views (everything)
%           bViewer == 2: only show GLR running view
%   bLogData - if the input data is log-transformed, if yes, mErr and
%           mAbsErr will be computed differently
%   PL - row vector of probability limits to be tested
%
% Outputs:
%   mErr - matrix of forecasting errors. size:(max forecast horizon * num obs)
%           Err = predicted - observed
%   mAbsErr - matrix of absolute forecasting errors
%           Abs Err = abs((predicted - observed)/observed)
%   cBounded - store the info if predicted is bounded by the probability
%           limits produced by the forecaster. nPL*1 cell array, each member
%           is a matrix (max forecast horizon * num obs)      
%   nAbn - number of paramter estimation routine (findSSEmin2())
%       abnormalies
%   nSwths - number of model switches (GLR)
%   rLikelikehood - log-likehoods (max forecast horizon * 1)

% probability limit levels for forecasters
nPL = length(PL);
if nPL == 0
    return;
end

dot_dat_pts = 24*7;

% The indices of the two probability limits that will be shown in the
% viewer
viewer_PL_index1 = ceil(nPL/2);
viewer_PL_index2 = nPL;

nFc = forecaster.getNfc(); % max forecasting horizon
nLB = forecaster.getNlb(); % look back length of the forecaster/SARI model
disp(['Look-back: ', num2str(nLB)]);

% prepare cell array of forecasters
cFc = cell(nPL, nFc+1);  
% cFc{iPL, end} is the current fc with PL(iPL) probability limits
% cFc{iPL, end-1} is lag-1 hindcaster, etc.
for iFcr = 1:nFc+1
    for iPL = 1:nPL
        cFc{iPL, iFcr} = forecaster.clone();
        cFc{iPL, iFcr}.setConflev(PL(iPL));
    end
end

% create glr buffer (parameter re-estimation window for alternative model)
% and alternative model
if strcmp(mode, 'glr')
    rBuf = zeros(1,m + nLB);
    mStrt = forecaster.getStrt();
end

% initialize viewer
if (bViewer)
    viewer = RTviewer('modeler', 0, nFc*20, ... % Raw data viewer window size
                    nFc * 4, ... % FHV window left panel size
                    nFc);  %  FHV window right panel size
end

% prepare performance measures

nObs = datasrc.getLen();
nErr = nObs - nLB - nFc; % discard prediction result at the beginning and the end
mErr = zeros(nFc, nErr);
mAbsErr = zeros(nFc, nErr);
cBounded = cell(1, nPL);
for iPL = 1:nPL
    cBounded{iPL} = zeros(nFc, nErr);
end
nAbn = 0; nSwths = 0;
rLikelihood = zeros(1, nFc);
if strcmp(mode, 'glr_sigma') % store summed log-likelihood for glr_sigma method
    rSLL0 = zeros(1, nErr); % Current summed log-likelihood 
    rSLL1 = zeros(1, nErr); % Alternative summed log-likelihood
    rRsd = zeros(1, nErr); % Residuals
end


iCd = 0; % data counter
iErr = 0; % performance counter
pLastSw = 0; % Last switch position to residual array - only used in glr_sigma
pRsd = 0; % Current position in residual array - only used in glr_sigma

global STOP    % the forecasting loop can be terminated externally

disp([' "." = ' num2str(dot_dat_pts) ' data points']);
while (strcmp(datasrc.getStatus(), 'normal'))
    if STOP == 1
        if bViewer
            viewer.destroy();
        end
        return;
    end
    cd = datasrc.getAnObs(); % current data
    iCd = iCd + 1;
    
    % display progress
    if ~mod(iCd, dot_dat_pts * 80)
        fprintf('\n');
    end
    if ~mod(iCd, dot_dat_pts * 10)
        fprintf('x');
    else
        if ~mod(iCd, dot_dat_pts)
            fprintf('.');
        end
    end
    
    % update the forecasters
    for iPL = 1:nPL
        cur_fc = cFc{iPL, end}.clone();
        cur_fc.update(cd);
        for iR = 2:nFc+1
            cFc{iPL, iR-1} = cFc{iPL, iR};
        end
        cFc{iPL, end} =  cur_fc;
    end
    
    if (bViewer == 1)
        viewer.update(cd, ...
            cFc{viewer_PL_index1, end}, cFc{viewer_PL_index2, end}, ... %FCV
            cFc{viewer_PL_index1, 1}, cFc{viewer_PL_index2, 1}, ... %HCV
            {cFc{viewer_PL_index1, :}}); %EMV
    end
    
    % Compute errors
    if iCd > nLB + nFc
        iErr = iErr + 1;
        for iFc = 1:nFc
            fc = cFc{1, end-iFc}.getFCs();
            sigma = cFc{1, end-iFc}.getSigma();
           
            if bLogData == 0
                mErr(iFc, iErr) = fc(iFc)-cd;
                if (cd ==0)
                    mAbsErr(iFc, iErr) = NaN;
                else    
                    mAbsErr(iFc, iErr) = abs((fc(iFc)-cd)/cd);
                end
            else
                mErr(iFc, iErr) = exp(fc(iFc))-exp(cd);
                if (cd == 0)
                    mAbsErr(iFc, iErr) = NaN;
                else
                    mAbsErr(iFc, iErr) = abs(mErr(iFc, iErr)/exp(cd));
                end
            end
            
            if iFc == 1
                if ~isnan(cd)
                    pRsd = pRsd + 1;
                    rRsd(pRsd) = cd - fc(1); % residuals
                end
            end
            
            if ~isnan(fc(iFc)-cd)
                rLikelihood(iFc) = rLikelihood(iFc) - log(sigma) ...
                  - (fc(iFc)-cd)^2/(2*sigma^2);
            end
            
            % with-in the PLs or not?
            for iPL = 1:nPL
                [llims, ulims] = cFc{iPL, end-iFc}.getLims();
                cBounded{iPL}(iFc, iErr) = (cd > llims(iFc)) && ...
                                           (cd < ulims(iFc));
            end
                    
        end
    end
    
    
    % fixed-slideing window GLR switching. With water demand data, this scheme
    % turns out to be no better than static method
    if strcmp(mode, 'glr') 
        rBuf = [rBuf(2:end) cd]; % update buffer
        if iCd >= nLB + m % buffer filled, alternative model can be estimated
            cur_sse = norm(calcAS2(rBuf,mStrt,cur_fc.getPara()))^2;
            %cur_sse = norm(calcAS2(log(rBuf),mStrt,cur_fc1.getPara()))^2;
            
            cur_sigma = cur_fc.getSigma();
            cur_score = m*log(cur_sigma) + cur_sse/(2*cur_sigma^2);
            alt_score = NaN;
            bSwitch = 0; % switch?
            
            [alt_para, alt_sigma, ErrorCode] = findSSEmin2(rBuf, mStrt, 0, ...
                cur_fc.getPara()); % use bFilter=1 if necessary
            %[alt_para, alt_sigma, ErrorCode] = findSSEmin2(log(rBuf), mStrt, 0, cur_fc1.getPara()); % use bFilter=1 if necessary
            if ErrorCode % msse routine abnormal
                if strcmp(mode, 'test')
                    nAbn = nAbn + 1;
                end
            else % new parameter found
                alt_sse = norm(calcAS2(rBuf,mStrt,alt_para))^2;
                
                %alt_sse = norm(calcAS2(log(rBuf),mStrt,alt_para))^2;
                alt_score = m*log(alt_sigma) + alt_sse/(2*alt_sigma^2);
                if cur_score - alt_score  > h
                    % model switch
                    bSwitch = 1;
                    for iPL = 1:nPL
                        % update forecasters
                        cFc{iPL, end} = SariForecaster(mStrt, ...
                            alt_para, alt_sigma, nFc, PL(iPL));
                        % copy forecaster state info
                        cFc{iPL, end}.setLookBack(cur_fc.getLookBack());
                    end
                    nSwths = nSwths + 1;
                end
                
            end
            if (bViewer)
                if (bViewer == 2)
                    viewer.dryrun();
                end
                viewer.updateGLR(cur_score, alt_score, bSwitch);
            end
        end
    end
    
    % General GLR scheme for adaptive sigma - model switching logic
    if strcmp(mode, 'glr_sigma') ...
        && iCd > nLB + nFc ... % wait till all forecasters initialized
        && ~isnan(cd) % missing observation - do nothing
          
        % update sum log-likelihood for current sigma
        rSLL0(pLastSw+1:pRsd) = rSLL0(pLastSw+1:pRsd) ...
            - log(sqrt(2*pi)*sigma) - rRsd(pRsd)^2/(2*sigma^2);
        % update sum log-likelihood for alt. sigma
        tmp = pRsd-pLastSw:-1:1;
        rSLL1(pLastSw+1:pRsd) = -0.5 * tmp .*  (1+...
          log(fliplr(cumsum(fliplr(rRsd(pLastSw+1:pRsd).^2)))./tmp * 2*pi));
       
        [max_value, pMax] = max(...
            rSLL1(pLastSw+1:pRsd)-rSLL0(pLastSw+1:pRsd));
        
        if max_value > h % model switch
            sigma1 = sqrt(sum(rRsd(pLastSw+pMax:pRsd).^2)/...
                (pRsd-(pLastSw+pMax)+1));
            
            pLastSw = pRsd; % update switching position
%             
%             fprintf('Model change detected at time %d, \n', iCd);
%             fprintf('(performance measure time %d), \n', iErr);
%             fprintf('sigma changed from %f to %f.\n', sigma, sigma1);
%            
            fprintf('sigma %f -> %f \n', sigma, sigma1);
            nSwths = nSwths + 1;
            
            for iFc = 1:nFc+1 % update all forecasters
                for iPL = 1:nPL
                    cFc{iPL, iFc}.changeSigma(sigma1);
                end
            end
        end
        
    end
                
    
end
    




end

