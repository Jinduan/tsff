function [ phi, theta, sigma, errorcode ] = findSSEmin( tsd, p, q )
%FINDSSEMIN Finds the minimum of the SSE surface for ARMA parameter estimation
%   This function uses a Newton-Raphson method to recursively calculate the ARMA
%   parameters phi and theta with minimum sum of square errors(SSE) or maximum 
%   likelihood (ML)
%INPUTS:
%   tsd -- input time series. tsd is treated as a stationary one.

ENDING_SL = 1e-4;  % Ending criterion for step_len
ENDING_DF = 1e-8;  % Ending criterion for differences of parameters
MAX_ITNS = 1000; % Maximum number of iterations allowed.

step_len = 0.1;  % Initial perturbance for jacob calculation
diff_b = inf;
term_flag = 1;  % 1- regular termination; 0 - can not find min inside the region

%[phi0, theta0, ~] = mom(tsd, p, q);  % Initial estimates by method of moments
% phi0 = [1.636364; -0.909091; 0.136364]; theta0 = [];
%

% Initialize phi and theta with all zeros
phi0 = zeros(p, 1); theta0 = zeros(q, 1);
phi = phi0; theta = theta0;

as0 = calcAS(tsd, phi0, theta0);  % Initial error vector
jac = zeros(length(as0), p+q);  % allocate mem for jacobian matrix

n_itns = 0;  % number of iterations

while (step_len > ENDING_SL) && (diff_b > ENDING_DF)
    % Calculate jacobian matrix
    for ii = 1:p+q
        phi = phi0; theta = theta0;
        if ii <= p
            phi(ii) = phi(ii) + step_len;
        else
            theta(ii-p) = theta(ii-p) + step_len;
        end
        as = calcAS(tsd, phi, theta);
        for jj = 1:length(as0)
            jac(jj, ii) = (as(jj) - as0(jj))/step_len;
        end
    end
    
    % Using multivariate linear regression to update {phi, theta}
    % linear regression gives the adjustment to orginial parameters
    b = [phi0; theta0] + regress(as0, -jac); 
    if p == 0
        phi = []; phi0 = []; theta = b;
    elseif q == 0
        theta = []; theta0= []; phi = b;
    else
        phi = b(1:p); theta = b(p+1:end);
    end
    
    if chkS(phi) && chkS(theta) % the model is stationary and invertable       
        diff_b = norm([theta-theta0; phi-phi0]);
        phi0 = phi; theta0 = theta; 
        as0 = calcAS(tsd, phi0, theta0);
        term_flag = 1;
    else
        step_len = step_len/2;
        term_flag = 0;
    end
    n_itns = n_itns + 1;
    if (n_itns > MAX_ITNS)
        term_flag = 2;
        break;
    end
end


errorcode = 0;   
if term_flag == 0
    warning('MATH:ILL-POSED_REGION', ...
        'The optimal seems to be on the boundary of the s/i region.');
    errorcode = 1;
elseif term_flag == 2
    warning('MATH:NUMERICAL INSTABILITY', ...
        'For some reason the diff_b jumps back and forth');
    errorcode = 2;
end

sigma = sqrt( nansum((as0).^2) / nansum(1-isnan(as0)) );  % Calc sigma

end

