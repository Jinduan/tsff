function [ s ] = chkS( phi )
%CHKS Check if a set of phi-weights is stationary. (also can be used to check invertibility)
%   phi is a column vector of autoregressive parameters.

if all(phi == 0)
    s=1;
    return;
end

rc_phiBrts = roots([-phi(end:-1:1)' 1]);

if any(phi(:)) && any(abs(rc_phiBrts) <= 1)
    s=0;
else
    s=1;
end

end

