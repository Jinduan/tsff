function [ rTs ] = makeTs( mStrt, rPara, sigma, nObs )
%MAKETS Generate time series realizations with given structure and
%parameter
%   
[rPolyPhi, rPolyTheta] = strt2poly(mStrt, rPara);

% rearrange PolyPhi and PolyTheta
rPolyPhi = rPolyPhi(end:-1:2);
rPolyTheta = rPolyTheta(end:-1:1);
    
% number of previous obs needed for forecasting/computing current obs
nPrevObs = length(rPolyPhi);

% number of previous errors/(a)s needed 
nPrevA = length(rPolyTheta)-1;

rtpObs = zeros(1, nPrevObs+nObs);

% random normal number generation
rtpA = normrnd(0, sigma, 1, nPrevA+nObs);

for iObs = 1:nObs
    rtpObs(iObs+nPrevObs) = -sum(rPolyPhi.*rtpObs(iObs:iObs+nPrevObs-1)) + ...
                            sum(rPolyTheta.*rtpA(iObs:iObs+nPrevA));
end

rTs = rtpObs(nPrevObs+1:end);




end

