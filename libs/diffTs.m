function [ rTs, mStrt ] = diffTs( rTs_in, mStrt_in )
%DIFFTS Apply the seasonal differencing operator to a time series
% INPUTS:
%       rTs_in: input time series. 
%       mStrt_in: msARI model structure matrix. must be valid
% RETURNS:
%       rTs: filtered time series
%       mStrt: new model structure w/o seasonality defined

if nargin < 2
    error('Not enough arguments.');
end
lb = lookback(mStrt_in);
if length(rTs_in) < lb
    error('cannot apply diff op - ts too short');
end
if all(mStrt_in(3, :) == 0) % already stationary
    rTs = rTs_in; mStrt = mStrt_in; return;
end

nS = size(mStrt_in, 2);
for iS = 1:nS
    S = mStrt_in(1, iS);
    D = mStrt_in(3, iS);
    for ii = 1:D
        rTs_in(1+S:end) = rTs_in(1+S:end) - rTs_in(1:end-S);
    end
end

rTs = rTs_in(lb+1:end);
mStrt = mStrt_in;
mStrt(2, :) = 0;


end

