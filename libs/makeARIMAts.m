function [ ts ] = makeARIMAts( sigma, phi, d, theta, varargin )
%MAKEARIMATS produce ARIMA(p,d,q) time series
%   produce ARIMA time series with standard dev of sigma,
%   autoregressive parameters of phi, integration level of d, and
%   moving average parameters of theta.
%   sigma is a scalar or a (1*n) vector with positive values,
%   n is the number of timeseries to be generated
%   phi is a (p*n) matrix with n parameter sets,  p <= 10
%   d is a scalar or a (1*n) vector of non-negative integers,  d <= 10
%   theta is a (q*n) matrix with n parameter sets, q <= 10
%   varargin{1} could be a positive integer denoting the length of a single
%   time series object generated, default value = 3k, max = 1M
%   ts is the resultant timeseries object with dimension(1*n).

MN = [mfilename() ':'];  % Module name (= function name)
MAX_NUM_OF_TS = 1000;   % Maximum number of timeseries allowed
MAX_LEN_OF_TS = 1000000;  % Maximum length of a time series
MAX_P_D_Q = 10;   % max allowable p, d, q
DEFAULT_LEN_OF_TS = int32(1000);  % Default length of time series
BURN_IN_LENGTH = 1000;  % burn-in period required for eliminate initial boundary effects

%% Input Check
n = size(phi, 2);
if (isscalar(sigma)); sigma = ones(1, n)*sigma; end;
if (isempty(varargin)); len = DEFAULT_LEN_OF_TS; else len = varargin{1}; end;

if any([size(d, 2) size(theta, 2)] ~= n)
    err = MException([MN 'DimUnmatch'], ...
        'One or more column size of sigma, phi, d, or theta does not match.');
    throw(err);
end

if ~n || n >= MAX_NUM_OF_TS
    err = MException([MN 'WrongNumOfTS'], ...
        'Number of timeseries is zero or too large.');
    throw(err);
end

if any(~[isa(d(:), 'integer'), isa(len, 'integer')])
    %    warning([MN 'RoundArg2Int'], ...
    %        'd or length are float/double, rounded to integer.');
    d = round(d);
    len = round(len);
end

if len < 0 || len > MAX_LEN_OF_TS
    err = MException([MN 'LenProblem'], ...
        'Length of TS is out of legal domain (between 0 and %d)', MAX_LEN_OF_TS);
    throw(err);
end

if any(sigma(:) <= 0)
    err = MException([MN 'SigmaNotPositive'], ...
        'Standard deviation must be larger than 0.');
    throw(err);
end

if any([size(phi, 1) size(theta, 1) d(:)'] < 0) || ...
        any([size(phi, 1) size(theta, 1) d(:)'] > MAX_P_D_Q)
    err = MException([MN 'p_d_q_outOfBoundary'], ...
        'p, d, or q is out of desirable domain (between 0 and %d)', MAX_P_D_Q);
    throw(err);
end

%% Check stationarity and invertibility

for ii = 1: n  %
 checkSI(phi(:, ii), theta(:, ii));
end


%% Time Series (TS) generation
tlen = len + BURN_IN_LENGTH;
a = normrnd(zeros(tlen, n), repmat(sigma, tlen, 1),  tlen, n); %White noises
z = zeros(tlen, n);  %time series data, pre alloc mem

p = size(phi, 1);  % NOTICE: This may not be the real p since trailing 0s may exist
q = size(theta, 1);  % may not be the real q

for ii = (max(p, q)+1):tlen
    z(ii, :) = diag(phi' * z(ii-1:-1:ii-p, :))' ...  % AR terms - phi weights
        - diag(theta' * a(ii-1:-1:ii-q, :))' ... % MA terms - theta weights
        + a(ii, :);
end % z = differenced time series (w) now

for ii = 1:n
    for jj = 1:d(ii)
        for kk = 2:tlen
            z(kk, ii) = z(kk, ii) + z(kk-1, ii);
        end
    end
end  % integration

z(1:BURN_IN_LENGTH, :) = [];
z = z - repmat(mean(z), len, 1);    %

ts = timeseries(z, 1:double(len), 'Name', 'ARIMA-Generated');

end

