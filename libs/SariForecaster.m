classdef SariForecaster < handle
    %SARIFORECASTSER Forecaster using a seasonal autoregressive integrated
    %model
    %   Detailed explanation goes here
    
    properties
        nFc = 10;  % maximum forecasting horizon
        conflev = 0.95;  % confidence level
        sigma = 1;
        rPolyPhi = 1;
        
        mStrt;
        rPara;
        
        rFc ;  % row vector of forecasts with diff. horizons
        rBounds; % row vector of 1/2 CI size with diff. horizons
        
        rLookBack; % row vector of the buffer for previous observations
        nLB; % size of look-back buffer
        
        bInit; % boolean. if lookback buffer is initialized?
    end
    
    methods
        %constructor, build a SARI model with certain forecasting horizons
        function [this] = SariForecaster(mStrt_in, rPara_in, sigma_in, nFc_in, conflev_in)
            % Inputs:
            %   mStrt - matrix showing the model structure
            %   rPara - row vector of parameters, length >= 1
            %   sigma_in - standard deviation of the white noise, >0
            %   nFc_in - max forecasting horizon, >= 1
            %   conflev_in - confidence level, >0, <1
            
            if nargin == 0
                return;
            end
            
            this.nFc = nFc_in;
            this.mStrt = mStrt_in;
            this.rPara = rPara_in;
            this.sigma = sigma_in;
            [this.rPolyPhi, ~] = strt2poly(this.mStrt, this.rPara);
            this.rPolyPhi = -this.rPolyPhi(end:-1:2); % move terms to RHS
            
            % allocate space for members
            this.nLB = length(this.rPolyPhi);
            this.rLookBack = zeros(1, this.nLB);
            this.bInit = 0;
            
            this.rFc = zeros(1, this.nFc);
            this.rBounds = zeros(1, this.nFc);
            
            this.setConflev(conflev_in);
      
          
        end
        
        function [] = setConflev(this, conflev_in)
            % set conflev and compute bounds
            if (conflev_in<0 || conflev_in>1) 
                return;
            end
            this.conflev = conflev_in;
            tpStrt = this.mStrt;
            tpStrt(3, :) = 0;  % model with no seasonality
            [tpPoly, ~] = strt2poly(tpStrt, this.rPara);
            tpBounds = deconv([1 zeros(1,length(tpPoly)-2+this.nFc)], tpPoly);
            this.rBounds = sqrt(cumsum(tpBounds.^2))* this.sigma * ...
                norminv(0.5 + this.conflev/2, 0, 1);
        end
        
        function [] = changeSigma(this, sigma_in)
            if sigma_in <=0 
                warning('Input sigma is not positive, sigma not changed.');
                return;
            end
            this.rBounds = this.rBounds / this.sigma * sigma_in;
            this.sigma = sigma_in;
        end
        
        
        function [] = update(this, new_zt_in)
            if ~this.bInit
                if isnan(new_zt_in)
                    return;  % do nothing
                else % initialize the buffer with first non-NaN obs
                    this.rLookBack = ones(1, this.nLB)*new_zt_in;
                    this.bInit = 1;
                end
            end
            
            if isnan(new_zt_in)
                % replace it with previous lag-1 forecast
                new_zt = this.rFc(1);
            else
                new_zt = new_zt_in;
            end
            
            % update lookback buffer
            this.rLookBack = [this.rLookBack(2:end) new_zt];
            
            % compute forecasts
            for iFc = 1:this.nFc
                if this.nLB >= iFc
                    this.rFc(iFc) = sum(this.rPolyPhi.* ...
                        [this.rLookBack(iFc:end), this.rFc(1:iFc-1)]);
                else
                    this.rFc(iFc) = sum(this.rPolyPhi.*...
                        this.rFc(iFc-this.nLB:iFc-1));
                end
            end
                
                    
        end
        
        function [nFc] = getNfc(this)
            nFc = this.nFc;
        end
        
        function [nLB] = getNlb(this)
            nLB = this.nLB;
        end
        
        function [mStrt] = getStrt(this)
            mStrt = this.mStrt;
        end
        
        function [rPara] = getPara(this)
            rPara = this.rPara;
        end
        
        function [sigma] = getSigma(this)
            sigma = this.sigma;
        end
        
        function [forecasts] = getFCs(this)
            forecasts = this.rFc;
        end
        
        function [rLookBack] = getLookBack(this)
            rLookBack = this.rLookBack;
        end
        
        function [] = setLookBack(this, rLookBack_in)
            if size(rLookBack_in)~=size(this.rLookBack)
                return;
            end
            this.rLookBack = rLookBack_in;
            this.bInit = 1;
            
            % compute forecasts
            for iFc = 1:this.nFc
                if this.nLB >= iFc
                    this.rFc(iFc) = sum(this.rPolyPhi.* ...
                        [this.rLookBack(iFc:end), this.rFc(1:iFc-1)]);
                else
                    this.rFc(iFc) = sum(this.rPolyPhi.*...
                        this.rFc(iFc-this.nLB:iFc-1));
                end
            end
        end
            
        
        function [lowerlims, upperlims] = getLims(this)
            lowerlims = this.rFc - this.rBounds;
            upperlims = this.rFc + this.rBounds;
        end
        
        function [lowerlims] = getLlims(this)
            lowerlims = this.rFc - this.rBounds;
        end
        
        function [upperlims] = getUlims(this)
            upperlims = this.rFc + this.rBounds;
        end
        
        function new = clone(this)
            % Instantiate new object of the same class.
            new = feval(class(this));
            
            % Copy all non-hidden properties.
            p = properties(this);
            for i = 1:length(p)
                new.(p{i}) = this.(p{i});
            end
        end
    end
    
end

