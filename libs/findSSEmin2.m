function [ rPara, sigma, ErrorCode ] = findSSEmin2( rObs_in, mStrt_in, bFilter, rPara0 )
%FINDSSEMIN2 Find minimum sum of squared error estimates for a given time
%series and model structure
%   find MSSE estimates of time series, version 2. The function is designed
%   to work on statioary Seasonal AR models (i.e., all(mStrt([3 4], :)==0));
%   Inputs:
%       rObs -  Row vector of observations, may contain NaN for missing obs
%       mStrt - Matrix showing SARIMA model structure, see strt2poly()
%       bFilter - if mStrt_in denotes non-stationary, whether apply
%               differecing operator before linear regression?
%               0 - no (default)
%               1 - yes
%       rPara0 - initial guess of parameters, row vector, default = zeros(1,nPara);
%   Outputs:
%       rPara - Row vector of MSSE parameter estimates
%       sigma - standard deviation
%       ErrorCode - 
%                   0 - No errors, rPara is valid.
%                   1 - Input not compatitable
%                   2 - Can not find a solution

% Algorithmic constants
CONVERGENCE_CRIT = 1e-5;  % Criterion of convergence
MIN_STEP_LEN = 1e-4;  % Minimum step length allowed
MAX_NO_ITERATION = 15;  % Maximum number of iterations allowed in a certain
                        % level of convergence criterion

% Input check
if nargin == 2
    bFilter = 0;
end

if size(mStrt_in, 1)~=4
    disp('mStrt must have 4 rows.');
    ErrorCode = 1;
    return;
end

if isempty(rObs_in)
    disp('No input observation series.');
    ErrorCode = 1;
    return;
end

% total number of linear parameters
nPara = sum(sum(mStrt_in([2 4], :)));

% Variable initialization/Memory allocation
if nargin<=3
    rPara = zeros(1, nPara); % Initial parameter estimates (all 0)
elseif length(rPara0)<nPara
    rPara = [reshape(rPara0, 1, []), zeros(1,nPara-length(rPara0))];
else
    rPara = rPara0;
end
rParaNew = zeros(1, nPara);

[rPolyPhi, rPolyTheta] = strt2poly(mStrt_in, rPara);
if max(length(rPolyPhi), length(rPolyTheta))>= length(rObs_in)
    disp('Observed series has less data points than that required by model structure.');
    ErrorCode = 1;
    return;
end

% if model structure is non-stationary, filter it first
if any(mStrt_in(3, :) ~=0) && bFilter %
    [rObs, mStrt] = diffTs(rObs_in, mStrt_in);
else
    rObs = rObs_in;
    mStrt = mStrt_in;
end

step = 0.1;  % Initial step length (perturbance for jacob matrix computation)
nItns = 0;   % Number of iteration in the current level
status = 0;  % status of the algorithm
regErr = 0;  % regression routine error

rA = calcAS2(rObs, mStrt, rPara);
mJab = zeros(length(rA), nPara);

while 1
    % Compute mJab: numerical Jacobian matrix
    for iPara = 1:nPara
        rTp = rPara;
        rTp(iPara) = rTp(iPara) + step;
        mJab(:, iPara) = (calcAS2(rObs, mStrt, rTp) - rA)'/step;
    end
   
    % Compute parameter adjustments
    rDPara = regress(rA', -mJab)';
    nItns = nItns + 1;
    
    % Converge?
    if norm(rDPara) < CONVERGENCE_CRIT
        if all(rDPara == 0)
            regErr = 1;  % linear regression routine error
        else
            break;  % convergence achieved;
        end
    end
        
    rParaNew = rPara + rDPara;
    rANew = calcAS2(rObs, mStrt, rParaNew);
    
    if norm(rANew)<norm(rA) && ...
            nItns < MAX_NO_ITERATION && ...
            ~regErr   
        % New set of parameters are valid, replace the original one
        status = 0;  % normal
        rA = rANew;
        rPara = rParaNew;
%        disp('New parameter sets accepted.');
%        disp(rPara);
    else % invalid move or non-convergence, try a smaller step length
        status = 2;  % given model structure yields non-stationary parameters
        step = step / 4;
        nItns = 0;  % reset iteration counter
%        disp('Try smaller step.');
        if step < MIN_STEP_LEN
            break;  % problems found
        end
    end
end
            
sigma = sqrt(mean(rA.^2));
ErrorCode = status;
        

end
        

