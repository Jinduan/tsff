function [ s ] = chkS2( mStrt, rPara )
%CHKS2 check if a SARIMA model is both stationary and invertible
%   Detailed explanation goes here

if any(mStrt(3, :) ~= 0)
    s = 0;
    return;
end

[ rPolyPhi, rPolyTheta ] = strt2poly( mStrt, rPara );

if any(roots(rPolyPhi(end:-1:1))<=1) || any(roots(rPolyTheta(end:-1:1))<=1)
    s = 0; % non-stationary or non-invertible
else
    s = 1; % stationary and invertible
end

end

