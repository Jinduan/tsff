function [out_ds] = readExl(in_xls_file, r)
%READEXL reads system-wide demands data in a folder of excel files
%   the excel files should have 
%   (1) identical column structures, 
%   (2) identical time intervals

%   in the columns of the spreadsheets, 
% Column (C),(E),and (G) are production flows to the sys (always >=0)
% Column (I) is the tank flow. >0 means tank to sys. 
% Total demand = (C)+(E)+(G)+(I)


out_mat_file_name = '__temp__.mat';

% Tempa SC only version
%data_folder_name = 'C:\\Users\\owner\\dropbox-wrwqrg-uc\\Dropbox\\2012\\2012\\South-Central';
%data_subfolder_name_list = {'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', ...
%    'SEPTEMBER', 'OCTOBER', 'November', 'December'};

% general verion
[data_folder_name, ~, ~] = fileparts(in_xls_file);
data_subfolder_name_list = {''}; %nothing here - no subfolder

data_file_name_pattern = 'SC_*';

demands = []; % demand data storage
n_demands = 0; % number of demand data points

for ifolder=1:numel(data_subfolder_name_list)
    file_name_list = ls(fullfile(...
        data_folder_name, ...
        data_subfolder_name_list{ifolder},...
        data_file_name_pattern));
    
    for ifile=1:size(file_name_list, 1)
        full_file_name = fullfile(...
            data_folder_name, ...
            data_subfolder_name_list{ifolder},...
            file_name_list(ifile, :));
        
        disp(['Pulling data from file ' file_name_list(ifile, :) ' ...']);
        temp = xlsread(full_file_name, 'Data', 'C3:I290');
        
        if (isempty(temp)) % read error
            disp('Error reading data file (check the file type).');
            continue;
        end
        
        if (size(temp, 2)<7)
            disp('Error: some data columns are completely missing.');
            continue;
        end 
        
        if (all(isnan(temp(:, 1)))) 
            disp('Error: LB Production flow for all time steps are Missing.');
            continue;
        elseif (all(isnan(temp(:, 3)))) 
            disp('Error: LSC Production flow for all time steps are Missing.');
            continue;
        elseif (all(isnan(temp(:, 5))))
            disp('Error: CH Production flow for all time steps are Missing.');
            continue;            
        elseif (all(isnan(temp(:, 7))))
            disp('Error: tank flow flow for all time steps are Missing.');
            continue;
        end
        
        demand_set = [temp(:, 1)  temp(:, 3)  temp(:, 5) temp(:, 7)];
        bad_values = sum(isnan(demand_set), 1); % How many missing in a col
        bad_rows = sum(isnan(demand_set), 2)>1; % two missing in one row
        
        if (any(bad_rows))
            disp(['Warning: flow for ' num2str(sum(bad_rows, 1)) ...
                ' rows cannot be calculated. NaN counts: ' ...
                num2str(bad_values) '. I am filling zeros there']);
            
            
        end
        demand_set(isnan(demand_set) & repmat(1-bad_rows, 1, 4))=0;
     
        
        demands = [demands; sum(demand_set,2)];
        n_demands = n_demands + size(temp, 1);
        disp('OK.');
    end           
    
end
%data = demands;
data = demands;
save(out_mat_file_name, 'data');
out_ds = FakeDataSource(out_mat_file_name, 0, r);
delete(out_mat_file_name);

end

