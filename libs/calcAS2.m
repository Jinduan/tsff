function [ rA ] = calcAS2( rObs,  mStrt, rPara)
%CALCAS2 calculate the errors associated with a set of model parameters
%   version 2 of the error calculation function. 

%   Input: 
%       rObs: row vector of observations. may contain NaNs. may not have
%           mean 0 so should be normalized before studying its autocorrelations
%       mStrt: matrix of model structure. 
%       rPara: a row vector of parameters, listed in the order as appeared
%           in the mStrt(2,1), mStrt(3,1), mStrt(2,2), mStrt(3,2), ...           
%   Output:
%       rA: row vector of model errors, those related to missing
%           observations are not included in rA. 
%       
%   approximations used:
%       1. observations prior to rObs(1) are set as mean(rObs).
%       2. errors prior to ra(1) are set as 0.
%       3. missing observations are approximated by the most recent
%       forecasts

[ rPolyPhi, rPolyTheta ] = strt2poly( mStrt, rPara );

% rearrange PolyPhi and PolyTheta
rPolyPhi = rPolyPhi(end:-1:2);
rPolyTheta = rPolyTheta(end:-1:2);
    
% number of previous obs needed for forecasting/computing current obs
nPrevObs = length(rPolyPhi);  % = nLB (lookback)

% number of previous errors/(a)s needed 
nPrevErr = length(rPolyTheta);

% number of obs
nObs = length(rObs);

% temporary row vector storing real and approximated observations
if all(mStrt(3, :) == 0) % stationary model, the raw data is shifted to around zero
    rtpObs = [zeros(1, nPrevObs), rObs-nanmean(rObs)];
else % non-stationary (integrated model)
    rtpObs = [zeros(1, nPrevObs), rObs];
end

% temp row vector storing errors (a's)
rtpErr = zeros(1, nPrevErr + nObs);

% Computing errors
% only 'a's corresponding to rObs(nPrevObs+1:end) are considered.
nA = nObs - nPrevObs - sum(isnan(rObs(nPrevObs+1:end)));
if nA < 1
    warning('CALCAS2:Input2short', ...
        'Input series for calcAS2() should have at least nLookBack+1 members');
    rA = [];
    return;
end
rA = zeros(1, nA);
pA = 1;
pErr = nPrevErr + 1;

%iterate thru all obs
for pObs = nPrevObs+1:length(rtpObs)
    
    if isnan(rtpObs(pObs))
        % if an observation is NaN, replace it with a forecast and carry on
        rtpObs(pObs) = sum(rPolyTheta.*rtpErr(pErr-nPrevErr:pErr-1)) - ...
                       sum(rPolyPhi.*rtpObs(pObs-nPrevObs:pObs-1));
        % the error is not taken into account
    else
        % observations is valid, calc errors
        rtpErr(pErr) = rtpObs(pObs) + sum(rPolyPhi.*rtpObs(pObs-nPrevObs:pObs-1)) ...
                       - sum(rPolyTheta.*rtpErr(pErr-nPrevErr:pErr-1));
        if pObs > 2*nPrevObs
            rA(pA) = rtpErr(pErr);
            pA = pA + 1;
        end
    end
    
    pErr = pErr + 1;
end

end

