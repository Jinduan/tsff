function [ rP, rScores ] = identifySarima( rObs, rPeriods, nPara, method )
%IDENTIFYSARIMA identify an msARI model structure using ACF diagram
%   All possible combinations of model structures are enumerated. The MSSE
%   estimates under each combination are computed. The theoretic ACs are
%   computed and compared to the sample ACs to determine the best model
%   structure.

%   Inputs:
%       rObs - row vector of observations, may contain NaN
%       rPeriods - row vector of periods (seasonality) used in the model
%                   structure, for hourly water demands, rPeriods=[1,24,168];
%       nPara - max number of linear parameters allowed in the model
%               the parameters can be distributed as AR or MA parameters
%               under different seasonalities.
%       method - method used for identification. 'aic' - Alaike Information
%         criteria; 'bic' - Bayesian information criteria (default); 'acf' 
%          - ACF diagram.
%   Outputs:
%       rP - row vector of best AR parameter distribution (see calcAS2())
%       rScores - row vector of min scores for 1, 2, ,....,nPara parameters

if nargin < 3
    error('Not enough arguments. Provide rObs, rPeriods, and nPara');
end
if nargin == 3
    method = 'bic';
end
disp(['identification method:' method]);
if nPara == 0
    error('Number of parameters must be larger than 0');
end
% max-lag autocorrelation to be compared. in ratio of total length of the observation series
AC_RATIO = 0.2;
nAC = min(floor(length(rObs) * AC_RATIO), 168*5);

nPeriods = length(rPeriods);
if nPeriods == 0
    error('Only one period, no need to identify parameter distributions.')
end
    
rSAC = zeros(1, nAC); % sample autocorrelations
rAC = zeros(1, nAC); % (sample) autocorrelations of the generated series

% enumerate all combinations of nPara = p1 + p2 +...+ p_nPeriods
% consider AR models
nBar = nPeriods-1;
%nBar = 2*nPeriods-1; % consider AR and MA

% compute reference autocorrelations
mobs = nanmean(rObs);
for iAC = 1:nAC
    rSAC(iAC) =  nansum( (rObs(1:end-iAC)-mobs) .* (rObs(1+iAC:end)-mobs) ) ...
        /nansum( (rObs-mobs) .* (rObs-mobs) );
end


rScores = zeros(1, nPara);
for iNPara = nPara:nPara % fixed n Para
    cellParaDiv = enumCombo(nan(1, nBar), iNPara, {});
    
    disp('Total number of model structures to be considered:');
    disp(length(cellParaDiv));
    
    % evaluate each potential model structure
    minScore = inf;
    minPara = [];
    
    nStrt = length(cellParaDiv);
    rScore = nan(1, nStrt);
    
    for iStrt = nStrt:-1:1
        % model structure
        tpParaDiv = cellParaDiv{iStrt};
        mStrt = [rPeriods; tpParaDiv; zeros(2, nPeriods)]; % AR models only
        
        disp(mStrt);
        [rPara, sigma, ErrorCode] = findSSEmin2(rObs, mStrt);
        
        if ErrorCode
            disp('Algorithmic error(s) found when computing MSSE');
        end
        
        score = 0;
        if strcmp(method, 'acf') == 1
            % generate a random time series to investigate ACs
            %rTs = makeTs(mStrt, rPara, sigma, length(rObs)*EXPERIMENT_MULTIPLIER);
%             for iAC = 1:nAC
%                 rAC(iAC) =  sum( (rTs(1:end-iAC)) .* (rTs(1+iAC:end)) ) ...
%                     /sum( rTs.^2 );
%                 score = score + sum((rAC(iAC)-rSAC(iAC)).^2); % deviations
%             end
            rAC = ywAC(mStrt, rPara, nAC);
            score = norm(rAC(1:nAC)-rSAC(1:nAC));
        else
            [ll, n_obs] = logLikelihood(rObs, mStrt, rPara, sigma);
            if strcmp(method, 'aic') == 1
                score = -2 * ll + 2 * nPara;
            elseif strcmp(method, 'bic') == 1
                score = -2 * ll + log(n_obs) * nPara;
            else
                error('Unknown identification scoring method');
            end
            
        end
        
        rScore(iStrt) = score;
        
        
        if score < minScore %&& all(mStrt(2,:)>0)
            minScore = score;
            minpStrt = iStrt;
            minPara = rPara;
            minSigma = sigma;
            %        minSts = rTs;
        end
    end
    
        
    rP = cellParaDiv{minpStrt};
    disp(['Mininum score: (', method, ') ' num2str(minScore)]);
    disp('Model structure:');
    disp(rP);
    disp('Parameters:');
    disp(minPara);
    disp(minSigma);
    
    % output all three scores for the criterion
    mStrt_temp = [rPeriods; rP; zeros(2, nPeriods)]; % AR models only
    if strcmp(method, 'acf') == 1
        rAC = ywAc(mStrt_temp, minPara, nAC);
    
        score_acf = norm(rAC(1:nAC)-rSAC(1:nAC));
    end
    [ll, n_obs] = logLikelihood(rObs, mStrt_temp, minPara, minSigma);
    score_aic = -2 * ll + 2 * length(minPara);
    score_bic = -2 * ll + log(n_obs) * length(minPara);
    
    if strcmp(method, 'acf') == 1
        disp(['Scores: ACF: ' num2str(score_acf) ', AIC: ' num2str(score_aic) ...
          ', BIC: ' num2str(score_bic)]);
    else
        disp(['Scores: AIC: ' num2str(score_aic) ...
          ', BIC: ' num2str(score_bic)]);
    end
        
    
    rScores(iNPara) = minScore;
    
end


end



function cellParaDiv = enumCombo(rBarPos, maxPos, cellParaDiv)
    % recursively list all combinations added up to nPara
    if ~any(isnan(rBarPos))
        % all positions determined -> a combination was found
        % transform bar positions to division sizes
        nDiv = length(rBarPos) + 1;
        rDivSize = zeros(1, nDiv);
        rDivSize(1) = rBarPos(1);
        for iDiv = 2:nDiv-1
            rDivSize(iDiv) = rBarPos(iDiv) - rBarPos(iDiv-1);
        end
        rDivSize(nDiv) = maxPos - rBarPos(nDiv-1);
        % append to the combo table
        cellParaDiv{end+1} = rDivSize;
        return;
    else
        iBar = 1;
        while ~isnan(rBarPos(iBar))
            iBar = iBar + 1;
        end
        if iBar == 1
            minPos = 0;
        else
            minPos = rBarPos(iBar - 1);
        end
        
        for iPos = minPos:maxPos
            rBarPos(iBar) = iPos;
            cellParaDiv = enumCombo(rBarPos, maxPos, cellParaDiv);
        end
    end
        
end