function [ ll, nObs ] = logLikelihood( rObs, mStrt, rPara, sigma )
%LIKELIHOODTS compute the log-likelihood of a msARI model on a time series.
%   INPUTS:
%       rObs - row vector of observations, may contain NaNs
%       mStrt - model structure matrix H
%       rPara - row vector of all AR parameters
%       sigma - standard deviation of the white noise
%   RETURNS:
%       ll - log-likelihood
%       nObs - number of observations used in the computation of ll

if nargin < 4
    error('Must have 4 arguments: rObs, mStrt, rPara, and sigma');
end
if sigma <= 0
    error('Sigma must be positive');
end

rPhi = fliplr(strt2poly(mStrt, rPara));
lenPhi = length(rPhi);
lenA = length(rObs) - length(rPhi) + 1; 
if lenA <= 0
    error('Not enough points in the time series');
end

ll = 0;
maxLB = mStrt(1, end)* length(rPara);
for iA = maxLB+1:length(rObs)
    a = dot(rPhi, rObs(iA-maxLB:iA-maxLB+lenPhi-1));
    if ~isnan(a)
        ll = ll - 0.5*log(2*pi) - log(sigma) - a^2/(2*sigma^2);
    end
end

% adjust for different look-back length lenPhi
ll = ll/lenA * length(rObs);
nObs = length(rObs) - maxLB;


end

