classdef EmuDataSource < handle
    %EMUDATASOURCE mimics a real-time data source that emits data at given interval
    %   the data source was initialized by a data file, it then gives out the
    %   data points in that file one by one at given time interval
    
    properties
        MN = 'EmuDataSource:';  % module name
        
        time_intvl = 3;  % time interval in seconds (default = 0)
        sum_ratio = 1; % the data source provides coarser temporal resolution
        % than the raw data by summing adjacent data
        
        status = 'uninit';
        % status of the data source:
        %   'uninit':   created but not initialized
        %   'normal':   initialized and ready to be read
        %   'end':      no more data is available
        %   'error':    some errors encountered, the data source is un-readable.
        
        ds_timer    % data source timer that controls data emitting
        
        dat_counter  % current data counter
        
        data % data, must be a vector
        
    end
    
    methods
        function [this] = EmuDataSource(datafile_name, time_intvl, sum_ratio)
            if nargin >= 3
                this.sum_ratio = round(abs(sum_ratio));
            end
            r = this.sum_ratio;
            if nargin >= 2
                this.time_intvl = time_intvl;
            end
            if nargin == 0
                throw(MException([this.MN 'NoDataFile'], ...
                    'Data file name must be provided to init this data source.'));
            end
            
            in = load(datafile_name);
            fn = fieldnames(in);
            dat = in.(fn{1});
            dat = reshape(dat, [], 1);
            n = length(dat);
            
            if n == 0
                this.status = 'error';
                return
            end
            
            dat = [dat; nan(mod(n,r),1)];
            dat = nanmean(reshape(dat, r, []), 1)';
            
            %%%%%%
            this.data = dat;
            
            
            this.dat_counter = 1;
            this.ds_timer = now;
            this.status = 'normal';

            
        end
        
       
        function [status] = getStatus(this)
            status = this.status;
        end
        
        function [len] = getLen(this)
            len = length(this.data);
        end
        
        function [dat] = getIdData(this) % get data for identification
            dat = reshape(this.data, 1, []);
        end
        
        function [obs] = getAnObs(this)
            if ~strcmp(this.status, 'normal')
                throw(MException([this.MN 'UnReadable'], ...
                    'The data source is not readable.'));
            end
            
            % this mechanism can only guarantee that interval is larger than
            % the given time interval
            %elapsed_time = (now - this.ds_timer) * 1e5;
            pause(this.time_intvl);
            %if elapsed_time  < this.time_intvl
%                pause(this.time_intvl - elapsed_time);
            %end
            
            obs = this.data(this.dat_counter);
            this.dat_counter = this.dat_counter + 1;
            if this.dat_counter > length(this.data)
                this.status = 'end';
            end
            
            this.ds_timer = now;
            
            
        end
        
        function [data_count, missing_data_count, data_mean,  ...
                data_variance, data_max, data_min] = getDataInfo(this)
            data_count = length(this.data);
            missing_data_count = sum(isnan(this.data));
            data_mean = nanmean(this.data);
            data_variance = nanvar(this.data);
            data_max = nanmax(this.data);
            data_min = nanmin(this.data);
        end
                
    end
    
end

