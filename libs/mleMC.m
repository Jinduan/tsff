function [phi, theta, sigma] = mleMC(ts, p, q)
% mleMC maximum likelihood estimation of ARMA model parameters using Monte-Carlo
% 

N = 2000; %total points sampled in the MC

ii = 1;
mins = inf; minph1 = 0; minph2 = 0;

while ii < N+1
    ph1 = unifrnd(-2, 2); ph2 = unifrnd(-1,1);
    if chkS([ph1;ph2])
        X(ii) = ph1; Y(ii) = ph2;
        Z(ii) = log(calcSSE(ts, ph1, ph2, 1));
        if Z(ii)< mins
            mins = Z(ii); minph1 = ph1; minph2 = ph2;
        end
        ii = ii + 1;
    end
    if ~mod(ii, 200); fprintf(1, '*'); end
end


fprintf(1, 'MOM estimates: ph1=%f, ph2=%f\n', ph1, ph2);
fprintf(1, 'Minimum(%f) obtained at (ph1, ph2)=(%f, %f)\n', ...
    mins, minph1, minph2);

    