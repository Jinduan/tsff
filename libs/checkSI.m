function [ ] = checkSI( phi, theta )
%CHECKSI check stationarity and invertibility
% plot reciprocals of the roots of phi(B)=0 and theta(B)=0, if they are all
% within the unit circle, then the ARMA process is stationary/invertible

MN = [mfilename() ':'];
N_UNIT_CIRCLE_POINTS = 200;

rc_phiBrts = 1./roots([-phi(end:-1:1)' 1]);

rc_thetaBrts = 1./roots([-theta(end:-1:1)' 1]);

if any(phi(:)) && any(abs(rc_phiBrts) >= 1)
    warning([MN 'TSnotStationary'], ...
        'The phi weights yield a non-stationary time series');
end
if any(theta(:)) && any(abs(rc_thetaBrts) >= 1)
    warning([MN 'TSnotInvertible'], ...
        'The set of theta weights yield a non-invertible time series');
end

unit_circle = exp(-1i*(linspace(0, 2*pi, N_UNIT_CIRCLE_POINTS)));

figure;
h = axes('NextPlot', 'add');
title('Roots of the characteristic functions');
plot(h, real(unit_circle), imag(unit_circle), 'k-', 'MarkerSize', 1);
plot(h, real(rc_phiBrts), imag(rc_phiBrts), 'xb', 'MarkerSize', 10);
plot(h, real(rc_thetaBrts), imag(rc_thetaBrts), '*r', 'MarkerSize', 10);
legend('|B| = 1', '\phi weights', '\theta weights', ...
    'Location', 'Southeast' );
% xlim(h, 1.5*max([abs(rc_phiBrts); abs(rc_thetaBrts)]')*[-1, 1]);
% ylim(h, get(h, 'XLim'));
axis equal square;

end

