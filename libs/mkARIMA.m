function [ tsdata ] = mkARIMA( sigma, phi, d, theta, len )
%MAKEARIMATS generate random vector from ARIMA(p,d,q) models
%   produce ARIMA random vector with standard dev of sigma,
%   autoregressive parameters of phi, integration level of d, and
%   moving average parameters of theta.
%   sigma is a scalar or a (1*n) vector with positive values,
%   n is the number of timeseries to be generated
%   phi is a (p*n) matrix with n parameter sets,  p <= 10
%   d is a scalar or a (1*n) vector of non-negative integers,  d <= 10
%   theta is a (q*n) matrix with n parameter sets, q <= 10
%   len could be a positive integer denoting the sizes of the
%   random vector generated, default value = 3k, max = 1M
%   ts is the resultant random vector with dimension(len*n).

if ~exist('len', 'var')
    len = 3000;
end
    
ts = makeARIMAts( sigma, phi, d, theta, len );
tsdata = ts.data;

end

