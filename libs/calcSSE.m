function [ SS ] = calcSSE( tsd, phi, theta, nocheck )
%CALCSSE calculates the SSE (sum of square errors) for a given set of parameter estimates
% The function uses an iterative method proposed by (Box & Jenkins 1979) pp216
% to compute the SSE for a given set of (phi, theta). The model generating the
% observed time series (tsd) is assumed to be an ARMA(p, q) process.
%
% INPUTS:
% tsd - observed time series data, must be a vector
% phi, theta - vectors of autoregressive and moving-average parameters to be tested


[AS] = calcAS(tsd, phi, theta, nocheck);

%SS = sum(AS(NB+2:end).^2);   % Conditional 
SS = sum(AS(:).^2);  % Unconditional (approximation)

end


