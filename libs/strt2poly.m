function [ rPolyPhi, rPolyTheta ] = strt2poly( mStrt, rPara )
%STRT2POLY transform model structure in matrix form to polynomial form
%    mStrt - matrix of model structures. The first row shows the lengths of
%           periods, the second row shows the number of AR parameters in each
%           level, the third row shows the order(s) of seasonal differencing 
%           operators, and the fourth row shows the number of MA parameters in
%           each level. For example, 
%               mStrt = [1,24,168;2,3,1;0,1,1;1,0,1]
%           denotes a double-seasonal model with two periods: 24 and 168
%           the model is ARIMA(2,0,1)*(3,1,0)_24*(1,1,1)_168
%   rPara - row vector of parameters

% number of parameters needed
nPara = sum(sum(mStrt([2, 4], :)));

% make sure rPara has enough size, fill with zeros if necessary
if length(rPara)<nPara
    rPara = [rPara, zeros(1,nPara-length(rPara))];
end

% row vectors representing AR and MA parameter polynomials.
rPolyPhi = 1; rPolyTheta = 1; 

% using convolution to calculate rPolyPhi and rPolyTheta
pPara = 1; % pointer to rPara
for iCol = 1:size(mStrt, 2)
    
    % temp matrixes
    nP = mStrt(2,iCol); nQ = mStrt(4,iCol); 
    nS = mStrt(1,iCol); nD = mStrt(3, iCol);
    tpPolyPhi = zeros(nS, nP);
    tpPolyTheta = zeros(nS, nQ);
    tpPolySeason = 1;
    
    tpPolyPhi(end, :) = rPara(pPara:pPara + nP - 1);
    pPara = pPara + nP;
    tpPolyTheta(end, :) = rPara(pPara:pPara + nQ - 1);
    pPara = pPara + nQ;
    
    tpPolyPhi = [1, -tpPolyPhi(:)'];
    tpPolyTheta = [1, -tpPolyTheta(:)'];
    
    for iD = 1:nD % make differencing operator
        tpPolySeason = conv(tpPolySeason, [1 zeros(1, nS-1) -1]);
    end
    
    % convolution for computing parameters
    rPolyPhi = conv(conv(rPolyPhi, tpPolyPhi), tpPolySeason);
    rPolyTheta = conv(rPolyTheta, tpPolyTheta);
end


end

