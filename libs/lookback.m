function [ lb ] = lookback( mStrt_in )
%LOOKBACK calculate the lookback length of a model structure
%INPUTS:
%       mStrt_in: msARI model structure (S,P,D) matrix
%RETURNS:
%       lb: lookback length
if nargin < 1
    error('Not enough arguments.');
end
lb = (mStrt_in(2,:)+mStrt_in(3,:)) * mStrt_in(1,:)';

end

