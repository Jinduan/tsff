function [acfs, pacfs, hf] = plotAcf( ts, acf_max_lag, acf_disp_itvl, show_raw_data )
%ACFTS plot ACF and PACF diagrams for time series with missing values
%INPUTS:
%   ts:
%       a vector showing the (observed) time series. missing values are NaN's.   
%   acf_max_lag:                    (default:30)
%       The max lags of ACs and PACs, 
%   acf_disp_itvl:          (default:1)
%       Only every (acf_disp_itvl)th AC/PACs are displayed. 
%       If the len is very large, AC/PACs may be packed together, use this
%       option to reduce the clutter (but losing info).
%   show_raw_data:          (default:1)
%       Show a graph of raw data(ts) at the very top of the generated plots
%RETURNS:
%   acfs, pacfs: row vectors of ACs and PACs
%   hf: handle of the figure

% Input check, assign defaults
if nargin < 4, show_raw_data = 1; end
if nargin < 3, acf_disp_itvl = 1; end
if nargin < 2, 
    error('Need at least two arguments: time series and max lag of ACF.');
end
if length(ts) < acf_max_lag
    error('Length of the time series is less than the required max lag of ACF');
end

z = reshape(ts, [], 1);
n = length(z);

%  computes ACF and PACF
acfs = ones( 1, acf_max_lag );  % store all ACF data
pacfs = ones( 1, acf_max_lag );

mz = nanmean(z); % mean disregard NaN
var = nansum((z-mz).*(z-mz));

% compute ACs
for iAC = 1:acf_max_lag
    acfs(iAC) =  nansum((z(1:end-iAC)-mz).*(z(1+iAC:end)-mz))/var;
end

% compute PACs
tppm = toeplitz([1, acfs]);
for iPAC = 1:acf_max_lag
    tppac = tppm(1:iPAC, 1:iPAC)\acfs(1:iPAC)';
    pacfs(iPAC) = tppac(end);
end

% plot ACF and PACF

hf = figure('Name','ACF and PACF Diagram'); 

if show_raw_data == 1
    subplot(2, 1, 1, 'Fontsize', 8); 
    plot(ts); xlabel(''); ylabel('{z}', 'Rotation', 0);
end

if acf_max_lag/acf_disp_itvl > 50
    mksize = 1;
else
    mksize = 3;
end

if show_raw_data == 1
    n_panel_column = 2;
    i_lpanel = 3; i_rpanel = 4;
    mkcolor = [0 0 1];
else
    n_panel_column = 1;
    i_lpanel = 1; i_rpanel = 2;
    mkcolor = [0 0 0];
end

% Draw ACF plot
spl = subplot(2, n_panel_column, i_lpanel, ...
    'XTick',0:acf_max_lag, 'FontSize',8);

stem(spl, 0:acf_disp_itvl:acf_max_lag, ...
    [1 acfs(acf_disp_itvl:acf_disp_itvl:end)],...
    'MarkerSize',mksize, 'markeredgecolor', mkcolor, 'color', mkcolor);
ylim(spl, [-1 1]);
ylabel('AC', 'Rotation', 0);
xlabel('Lag time');

spr = subplot(2, n_panel_column, i_rpanel, ...
    'XTick',0:acf_max_lag, 'FontSize',8);
stem(acf_disp_itvl:acf_disp_itvl:acf_max_lag, ...
    pacfs(acf_disp_itvl:acf_disp_itvl:end), ...
    'MarkerSize', mksize, 'markeredgecolor', mkcolor, 'color', mkcolor);
ylim(spr, [-1 1]);
ylabel('PAC', 'Rotation', 0);
xlabel('Lag time');

%if strcmpi(get(gcf, 'WindowStyle'), 'docked') == 0
%    set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
%end

end        





